package unit8.ex6;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class MyClass {
    public static boolean isAlphabetic(String s) {
        return IntStream.iterate(0, w -> w+1)
                .limit(s.length())
                .allMatch((a) -> Character.isAlphabetic(s.charAt(a)));
    }

    public static void main(String[] args) {
        //letters("Sasha").forEach(System.out::println);
        System.out.println(isAlphabetic("Sassha"));
        System.out.println(isAlphabetic("Sassha12"));
    }
}
