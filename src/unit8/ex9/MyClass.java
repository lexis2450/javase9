package unit8.ex9;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MyClass {
    public static void main(String[] args) throws IOException {
        String contents = new String(Files.readAllBytes(Paths.get("C:\\Users\\alsa1118\\IdeaProjects\\javase9\\src\\unit8\\ex9\\lol")), StandardCharsets.UTF_8);
        List<String> words = new ArrayList<>(Arrays.asList(contents.split("\\PL+")));

        Set<Character> vowels = new HashSet<Character>();
        for (char ch : "aeiou".toCharArray()) {
            vowels.add(ch);
        }
        boolean flag;
        for (String a : words) {
            flag = true;
            for (Character b : vowels){
                if(!a.contains(b.toString())) {
                    flag = false;
                }
            }
            if(flag) System.out.println(a);
        }

    }
}
