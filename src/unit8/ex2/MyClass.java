package unit8.ex2;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class MyClass {
    public static void main(String[] args) throws IOException {
        String contents = new String(Files.readAllBytes(Paths.get("C:\\Users\\alsa1118\\IdeaProjects\\javase9\\src\\alice.txt")), StandardCharsets.UTF_8);
        List<String> words = new ArrayList<>(Arrays.asList(contents.split("\\PL+")));
        double start = System.currentTimeMillis();
        words.stream().filter(w -> w.length() > 5).toArray();
        double stop = System.currentTimeMillis();
        double time1 = stop - start;

        start = System.currentTimeMillis();
        words.parallelStream().filter(w -> w.length() > 5).toArray();
        stop = System.currentTimeMillis();
        double time2 = stop - start;
        Logger.getGlobal().info(Arrays.deepToString(words.stream().filter(w -> w.length() > 5).limit(5).toArray()));
        Logger.getGlobal().info("1: " + time1 + '\n' + "2: " + time2);
        //затестить на Война и мир
    }
}
