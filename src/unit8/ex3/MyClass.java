package unit8.ex3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class MyClass {
    public static void main(String[] args) {
        int[] value = {1, 4, 9, 16};
        Stream<int[]> value1 = Stream.of(value);
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < value.length; i++) {
            list.add(value[i]);
        }
        Stream<Integer> str = list.stream();
    }
}
