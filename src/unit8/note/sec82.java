package unit8.note;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class sec82 {
    public static void main(String[] args) throws IOException {
        String contents = new String(Files.readAllBytes(
                Paths. get ("C:\\Users\\Александра\\IdeaProjects\\JavaSE9\\src\\alice.txt")), StandardCharsets. UTF_8);
        //List<String> words = Arrays.asList(contents.split("\\PL+"));

        Stream<String> words = Stream.of(contents.split("\\PL+"));
        Stream<String> song = Stream.of("gently", "down", "the", "stream");
        Stream<String> silence = Stream.empty();
        //для создания бесконечного потока данных
        Stream<String> echos = Stream.generate(() -> "Echo");
        Stream<Double> randoms = Stream.generate(Math::random);

        Stream<BigInteger> integers = Stream.iterate(BigInteger.ZERO, n -> n.add(BigInteger.ONE)) ;
        BigInteger limit = new BigInteger("1000000");
        /*Stream<BigInteger> integers1 = Stream.iterate(BigInteger.ZERO,
                n -> n.compareTo(limit) < 0,
                n -> n.add(BigInteger.ONE));*/
    }
}
