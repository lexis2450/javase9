package unit8.note;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class sec81 {
    public static void main(String[] args) throws IOException {
        String contents = new String(Files.readAllBytes(
                Paths. get ("C:\\Users\\Александра\\IdeaProjects\\JavaSE9\\src\\alice.txt")), StandardCharsets. UTF_8);
        List<String> words = Arrays.asList(contents.split("\\PL+"));
        int count = 0;
        for(String w : words){
            if(w.length() > 12){
                count++;
            }
        }
        System.out.println(count);

        long count1 = words.stream().filter(w -> w.length() > 12).count();
        long count2 = words.parallelStream().filter(w -> w.length() > 12).count();
        List<String> a = new ArrayList<>();
        //a.add("")
        //Stream<String> a1 = Stream.of()
        //Stream<String> combined = Stream.concat( Arrays.stream((Character[])"Hello".toCharArray()), ("World"));
    }
}
