package unit8.note;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class sec83 {
    public static void main(String[] args) throws IOException {
/*        String contents = new String(Files.readAllBytes(
                Paths.get ("C:\\Users\\Александра\\IdeaProjects\\JavaSE9\\src\\alice.txt")), StandardCharsets. UTF_8);

        List<String> words = Arrays.asList(contents.split("\\PL+"));
        Stream<String> longWords = words.stream().filter(w -> w.length() > 12);
        Stream<String> longWords1 = words.stream().map(String::toLowerCase);
        Stream<String> longWords2 = words.stream().map(s -> s.substring(0, 1));
        Stream<Stream<String>> result = words.stream().map(w -> codePoints(w));
        Stream<String> flatResult = words.stream().flatMap(w -> codePoints(w));*/
                Stream.concat( codePoints("Hello"), codePoints("World")).distinct().forEach(System.out::println);
    }
    public static Stream<String> codePoints(String s) {
        List<String> result = new ArrayList();
        int i = 0;
        while(i < s.length()) {
            int j = s.offsetByCodePoints(i, 1);
            result.add(s.substring(i, j));
            i = j;
        }
        return result.stream();
    }

}
