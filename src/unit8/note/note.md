Потоки данных
===
####8.1. От итерации к потоковым операциям
Для обработки коллекций требуется ее перебирать и выполнить над каждым элементам операцию
см пример 81
Можно делать с помощью потоков.
stream()
parallelStream()
Отличия от коллекций:
- поток данных не хранит свои элементы (они хранятся в основной коллекции)
- потоковые операции не изменяют источник данных(для результата формируется новый поток)
- поток операции не выполняются, пока не потребуется результат

Три стадии потоков
 - создание потока stream()
 - указание промежуточных операций filter(), parallelFilter()
 - выполнение оконечной операции count()
 
 #### 8.2. Создание потоков данных
 Любую коллекцию можно преобразовать в поток данных
 stream() - из интерфеса Collection
 Stream.of() - для массивов
 Stream.of(array, from, to) - для массивов
 см пример 82
    Stream<String> words = Stream.of(contents.split("\\PL+"));
    Stream<String> song = Stream.of("gently", "down", "the", "stream");
    Stream<String> silence = Stream.empty();
    
   для создания бесконечного потока данных
    Stream<Double> randoms = Stream.generate(Math::random);
         
 Stream<BigInteger> integers = Stream.iterate(BigInteger.ZERO, n -> n.add(BigInteger.ONE)) ;
 BigInteger limit = new BigInteger("1000000");
 Stream<BigInteger> integers1 = Stream.iterate(BigInteger.ZERO, 
 **n -> n.compareTo(limit) < 0,** 
 n -> n.add(BigInteger.ONE));*/
 
 #### Методы filter(), map() и flatMap()
 В результате  преобразования потока получается другой поток
 map() - для преобразования потока, передаем ему функцию или лямбда выражение
 flatMap() - аналогична map()только разница в том, что операция map создает 
 одно выходное значение для каждого входного значения, 
 тогда как операция flatMap производит произвольное число (ноль или более) 
 значений для каждого входного значения.
 
 ####8.4. Извлечение и соединение потоков
 поток.limit(n) - возвращает поток данных, оканчивающийся после n элементов
 Stream<Double> randoms = Stream.generate(Math::random).limit(100);
 
 А в результате вызова поток, skip (п) отбрасываются первые п элементов
 Stream<String> words = Stream.of(contents.split("\\PL+M)).skip(1);
 избавляются от ненужной первой строки
 
 поток.takeWhile(предикат)
 из потока данных принимаются все элементы до тех пор пока условие-предикат не станет истинным
 
 dropWhile()
 противоположно, пропускаэт элементы, пока условие истино
 
 concat() соединение двух потоков данных вместе
 Stream<String> combined = Stream.concat( letters("Hello"), letters("World"));
 // Получается следующий поток данных:
 // ["Н", "е", "1", "1", "о", "W", "о", "г", "1", "d"]
 
 ####8.5. Другие потоковые преобразования
 distinct() - убирает дубликаты из потока
 
 sorted() - для сортировки потоков данных
 несколько вариантов
 1 для элементов типа Comparable
 2 принимает в качетве параметра тип Comparator
 Stream<String> longestFirst =
 words.stream().sorted(Comparator.comparing(String::length).reversed());
 
 peek()
 выдает другой поток данных с теми же самыми элементами, что и у исходного потока, 
 но передаваемая ему функция вызывается всякий раз, когда извлекается элемент
 Object[] powers = Stream.iterate(1.О, p -> p * 2).peek(e -> System.out.println("Fetching " + e))
    .limit(20).toArray();
    
  удобно использовать для отладки программы
 .peek(x -> {)  
    return;})
    
 ####8.6. Простые методы сведения
 выполняют оконечные операции сведения потоковых данных к непотоковому значению
 count()
 max(), min()
 эти методы возвращают тип Optional<T>
 findFierst()
 findAny()
 anyMatch() - имеются ли вообще совпадения
 boolean aWordStartsWithQ = words.parallel().anyMatch(s -> s.startsWith("Q"));
 allMatch() и noneMatch() - с предикатом совпадют все или ни один 
 
 ###8.7. Тип Optional
 Объект типа Optional<Т> служит оболочкой для объекта обобщенного типа Т 
 или же ни для одного из объектов. В первом случае считается, что значение присутствует.
 Более надежно т к могут быть нулевые ссылки
 
 ####8.7.1 Как обращаться с необязательными значенииями
 Для эффективного применения типа Optional самое главное — выбрать метод, который возвращает 
 альтернативный вариант, если значение отсутствует, или употребляет значение, если только оно присутствует.
     String result = optionalString.orElse("");
     // Заключенная в оболочку строка, а в ее отсутствие - пустая строка ""
     
  можно использовать спец функцию по умолчанию
  String result = optionalString.orElseGet(() ->
  System.getProperty("user.dir"));
  // Функция вызывается только по мере надобности
  
  можно генерировать исключения при отсутствии значения
  String result = optionalString.orElseThrow(IllegalStateException::new);
  // предоставить метод, который выдает объект исключения
  
  Если объект optionalString не содержит значения, то в переменную res помещается оболочка со значением..
  
  ifPresent() - применить сгенерированное значение если оно не пустое
  optionalValue.ifPresent(v -> Обработать v);
  
  optionalValue.ifPresentOrElse(  //если объект содержит значение - делает первое
  v -> Обработать v,
  () -> сделать что то другое;
  
  ####8.7.2. Как не следует обращаться с необязательными значениями
  Метод is Present () извещает, содержит ли значение объект типа Optional<T>
  
  if (optionalValue.isPresent()) optionalValue.get().someMethod();
  
  не проще, чем выражение 
  if (value != null) value.someMethod();
  
  ####8.7.3. Формирование необязательных значений
  если самому создавать объект типа Optional
  есть несколько статических методов
  Optional.of(result)
  Optional.empty()
  
  метод- мост
  public static Optional<Double> inverse(Double x) {
  return x == 0 ? Optional.empty() : Optional.of(1 / x);
  }
  
  ####8.7.4. Сочетание функций необязательных значений с методом flatMap()
  имеется метод f (), возвращающий объект типа Optional<T>,
   у целевого типа Т — метод g (), возвращающий объект типа Optional<U>.
  Если бы это были обычные методы, их можно было бы составить в вызов s. f () . g (). 
Нельзя т к результат вызова s. f () относится к типу Optional<T>, а не к типу Т. 
Вместо этого нужно сделать следующий вызов:
  Optional<U> result = s.f().flatMap(T::g);
 flatMap() - служит для сведения двух птоков в один
 
 ####8.7.5. Преобразование необязательного значения в поток данных
 Stream<User> users = ids.map(Users::lookup).flatMap(Optional::stream);
 Optional::stream - возвращает 0 или 1
 flatMap - объединяет все элементы вместе
 несуществующие пользователи просто исключаются
 
 ###8.8. Накопление результата
 если необходимо посмотреть полученные результаты
 iterate() - устар
 forEach()
 stream.forEach(System.out::println);
 в параллельном потоке элементы перебираются вразноброс
 forEachOrdered() - по порядку
 toArray() - получ элементы перевести в массив Object[]
 метод collect()
 
 List<String> result = stream.collect(Collectors.toList())
 Set<String> result = stream.collect(Collectors.toSet())

 TreeSet<String> result = stream.collect(Collectors.toCollection(TreeSet::new));

 String result = stream.collect(Collectors.joining());
 
 String result = stream.collect(Collectors.joining(", "));

 String result = stream.map(Object::toString).collect(Collectors.joining(", ")) ;
 
 //если вычисляем сумму или статистику
 IntSummaryStatistics summary = stream.collect(
 Collectors.summarizinglnt(String::length)); 
 double averageWordLength = summary.getAverage(); 
 double maxWordLength = summary.getMax();
 
 ###8.9. Накопление результатов в отображении
 если необходимо накапливать данные в отображении
 
 метод Collectors.toMap();//принимает ф-ии получения ключа и знаения
 Map<Integer, String> idToName = people.collect(
    Collectors.toMap(Person::getld, Person::getName))
 
 в качестве второго аргумента м б Function.identity()
 Map<Integer, Person> idToPerson = people.collect (
    Collectors.toMap(Person::getld, Function.identity()));
    
 если одному ключу соответствует несколько значений - исключение
 IlligalStateException
 
 Map<String, Set<String>> countryLanguageSets = locales.collect (
    Collectors.toMap(
        Locale::getDisplayCountry,
            1 -> Collections.singleton(1.getDisplayLanguage()),
                 (a, b) -> { // объединить множества а и b 
                 Set<String> union = new HashSeto (a);
                  union.addAll(b);
                  return union; }));
                  
 ###8.9. Группировка и разделение
 Часто значения с одинаковыми характеристиками образуют группы
 groupingBy()
 Map<String, List<Locale>> countryToLocales = locales.collect(
                 Collectors.groupingBy(Locale::getCountry));
                 
 если функция-классификатор предикатная(возвр лог знач), элементы потока данных разделяются на 
 основной и побочный списки
 выгодно использовать partitioningBy();
 
 ###8.11. Нисходящие коллекторы
groupingBy() формирует множество, значениями которого являются списки
следует предоставить нисходящий коллектор для их обработки. 

если вместо списков требуются множества Collectors .toSet ()
 Map<String, Set<Locale>> countryToLocaleSet = locales.collect( groupingBy(Locale::getCountry, toSet()));
 
 сведения сгруппированных элементов к числам
  - counting () — производит подсчет накопленных элементов. 
 Map<String, Long> countryToLocaleCounts = locales.collect ( groupingBy(Locale::getCountry, counting()));
 
 - summing (Int | Long | Double) — принимает в качестве аргумента функцию
 Map<String, Integer> stateToCityPopulation = cities.collect( 
    groupingBy(City::getState, summinglnt(City::getPopulation)) ) ;
    
 - maxBy () и minBy () — принимают в качестве аргумента компаратор 
 Map<String, City> stateToLargestCity = cities.collect( groupingBy(City::getState,
     maxBy(Comparator.comparing(City::getPopulation))) ) ;
 
 - mapping () — применяет функцию к результатам, полученным из нисходящего потока данных
 Map<String, OptionaKString» stateToLongestCityName = cities .collect ( 
            groupingBy(City::getState, mapping(City::getName,
            maxBy(Comparator.comparing(String::length)))));
 • В каждом штате получаются названия городов, которые сводятся по максимальной длине. 
 
 ###8.12. Операции сведения
 reduce () реализует общий механизм для вычисления значения из потока данных
 List<Integer> values = ...;
 Optional<Integer> sum = values.stream().reduce((x, y) -> x + y);
 Практическую пользу могут принести многие ассоциативные операции, в том числе сложение, 
 умножение, сцепление символьных строк, получение максимума и минимума, объединение 
 и пересечение множеств. Примером операции, которая не является ассоциативной, 
 служит вычитание
 
 ###8.13. Потоки данных примитивных типов
 До сих пор цеочисленные значения накапливались в оолочках
 но есть спец классы IntStream, LongStream и DoubleStream
 
 Чтобы создать поток данных типа IntStream
 IntStream stream = IntStream.of(1, 1, 2, 3, 5); 
 stream = Arrays.stream(values, from, to);
 // массив values относится к типу int[]
 
 Поток объектов можно преобразовать в поток данных примитивных типов с помощью методов 
 mapToInt (), mapToLong (() или mapToDouble (). 
 если имеется поток символьных строк и их длины требуется обработать как целочисленные значения, 
 это можно сделать и средствами класса IntStream следующим образом:
 Stream<String> words = ...;
 IntStream lengths = words.mapToInt(String::length);
 
 Чтобы преобразовать поток данных примитивного типа в поток объектов boxed () 
 Stream<Integer> integers = IntStream.range(0, 100).boxed();
 
 ###8.14. Параллельные потоки данных
 Stream<String> parallelWords = words.parallelStream();
 промежуточные операции распараллеливаются
 при распараллеливании операции не должны зависеть друг  от друга
 без созранения состояний
 параллельный и послед код должен приводить к одинак результату
 Stream.unordered() - порядок не имеет значения
 
   
 
 
 
 
