package unit8.ex16;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.stream.Stream;

public class MyClass {
    public static void main(String[] args) {
        //BigInteger a = new BigInteger(1000000000000000000000000000000000000000000000000000);
        double start = System.currentTimeMillis();
        System.out.println(Arrays.toString(Stream.iterate(BigInteger.valueOf((long)(Math.pow(10, 1000))),
                x -> x.add(BigInteger.valueOf(1))).parallel().filter((x -> x.isProbablePrime(1)))
                .limit(500).toArray()));
        double stop = System.currentTimeMillis();
        double time1 = stop - start;
        System.out.println(time1);

        start = System.currentTimeMillis();
        System.out.println(Arrays.toString(Stream.iterate(BigInteger.valueOf((long)(Math.pow(10, 1000))),
                x -> x.add(BigInteger.valueOf(1))).filter((x -> x.isProbablePrime(1)))
                .limit(500).toArray()));
        stop = System.currentTimeMillis();
        double time2 = stop - start;
        System.out.println(time2);
        }
}
