package unit8.ex11;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class MyClass {
    public static void main(String[] args) {
        List<String> str = new ArrayList<>();
        str.add("lololololol");
        str.add("Everything you can imagine is real.");
        str.add("And, when you want something, all the universe conspires in helping you to achieve it.");
        Optional a = str.stream().map(w -> w.length()).max(Comparator.comparing(Integer::valueOf));
    }
}
