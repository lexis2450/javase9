package unit8.ex7;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class MyClass {
    public static boolean isAlphabetic(String s) {
        return IntStream.iterate(0, w -> w+1)
                .limit(s.length())
                .allMatch((a) -> Character.isAlphabetic(s.charAt(a)));

    }


    public static void main(String[] args) throws IOException {
        String contents1 = new String(Files.readAllBytes(Paths.get("C:\\Users\\alsa1118\\IdeaProjects\\javase9\\src"
                + "\\alice.txt")), StandardCharsets.UTF_8);
        List<String> words1 = new ArrayList<>(Arrays.asList(contents1.split("\\W")));
        //words1.stream().filter(s -> isAlphabetic(s)).limit(10).forEach(System.out::println);

        String contents = new String(Files.readAllBytes(Paths.get("C:\\Users\\alsa1118\\IdeaProjects\\javase9\\src"
                + "\\alice.txt")), StandardCharsets.UTF_8);
        List<String> words = new ArrayList<>(Arrays.asList(contents.split("\\W")));
        Stream<String> streamWords = words.stream().filter(a -> isAlphabetic(a)).map(String::toLowerCase);

        Map<String, Long> map = streamWords.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(10)
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue
                ));

        map.forEach((k, v) -> System.out.println(k + "=" + v));

    }
}
