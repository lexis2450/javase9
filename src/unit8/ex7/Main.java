package unit8.ex7;

public class Main {

    public static void main(String[] args) {
        java.util.Map<Integer, String> map = new java.util.HashMap<>();
        map.put(1, "Maksim");
        map.put(2, "Sasha");
        map.put(3, "Polina");
        System.out.println(map);
        for(java.util.Map.Entry<Integer, String> a: map.entrySet()){
            System.out.println(a);
        }
    }
}
