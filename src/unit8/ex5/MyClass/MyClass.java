package unit8.ex5.MyClass;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class MyClass {
    public static Stream<String> codePoints(String s) {
        List<String> result = new ArrayList();
        for (int i = 0; i < s.length(); i++) {
            result.add(s.substring(i, i + 1));
        }
        return result.stream();
    }

    public static Stream<String> letters11(String s) {
        return IntStream.iterate(0, w -> w+1).limit(s.length()).mapToObj(w -> String.valueOf(s.charAt(w)));
    }

    public static void main(String[] args) {
        //letters("Sasha").forEach(System.out::println);
        letters11("Sassha").forEach(System.out::println);
    }
}
