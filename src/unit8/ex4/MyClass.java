package unit8.ex4;

import java.util.Arrays;
import java.util.stream.Stream;

public class MyClass {
    public static Stream<Long> method(int a , int c, int m){
        Stream<Long> res = Stream.iterate(0L, n -> (((a * n) + c) % m)).limit(100);
        return res;
    }

    public static void main(String[] args) {
        method(2, 5,7).forEach(System.out::println);
        //System.out.println(Arrays.deepToString(a).limit(5).toArray()));

    }
}
