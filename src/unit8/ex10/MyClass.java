package unit8.ex10;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class MyClass {
    public static void main(String[] args) {
        List<String> str = new ArrayList<>();
        str.add("Alex");
        str.add("lol");
        str.add("prive");
        Double a = str.stream().mapToInt(w -> w.length()).average().getAsDouble();
        System.out.println(a);

    }
}
