package unit8.ex17;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static unit8.ex6.MyClass.isAlphabetic;

public class MyClass {
    public static void main(String[] args) throws IOException {
        String contents = new String(Files.readAllBytes(Paths
                .get("C:\\Users\\Александра\\IdeaProjects\\JavaSE9\\src\\unit8\\note\\note.md")), StandardCharsets.UTF_8);
        List<String> words = new ArrayList<>(Arrays.asList(contents.split("\\PL+")));
        Stream<String> streamWords = words.stream();
        streamWords.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().parallelStream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).limit(500)
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue))
                .forEach((s, count) -> System.out.println(s + ": " + count));
        //map.forEach((k, v) -> System.out.println(k + "--->" + v));
    }
}
