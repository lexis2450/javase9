package operations;

public class Main {



    public static void main(String[] args) {
        int x = 13420;
        int x1 = -13420;
        System.out.println("x = "+x);
        System.out.println("-x = "+x1);
        System.out.println("x = "+Integer.toBinaryString(x));
        System.out.println("-x = "+Integer.toBinaryString(x1));
        System.out.println("--------------------");
        System.out.println("x = "+~x);
        System.out.println("~x = "+Integer.toBinaryString(x));
        System.out.println("----------------------");
        int y = 20;
        System.out.println("x = "+x);
        System.out.println("x = "+y);
        System.out.println("x = "+Integer.toBinaryString(x));
        System.out.println("y = "+Integer.toBinaryString(y));
        System.out.println("x & y = "+Integer.toBinaryString(x&y));
        System.out.println("----------------------");
        System.out.println("x = "+Integer.toBinaryString(x));
        System.out.println("y = "+Integer.toBinaryString(y));
        System.out.println("x | y = "+Integer.toBinaryString(x|y));
        System.out.println("----------------------");
        System.out.println("x = "+Integer.toBinaryString(x));
        System.out.println("y = "+Integer.toBinaryString(y));
        System.out.println("x ^ y = "+Integer.toBinaryString(x^y));
        System.out.println("----------------------");
        System.out.println("x = "+Integer.toBinaryString(x));
        int xtemp = x << 2;
        System.out.println("x « 2 = "+Integer.toBinaryString(x<<2));
        System.out.println("x = " + xtemp);
        System.out.println("----------------------");
        System.out.println("x = "+Integer.toBinaryString(x));
        xtemp = x << 2;
        System.out.println("x » 2 = "+Integer.toBinaryString(x>>2));
        System.out.println("x = " + xtemp);
        System.out.println("----------------------");
        System.out.println("x = "+Integer.toBinaryString(x));
        xtemp = x << 3;
        System.out.println("x >>> 1 = "+Integer.toBinaryString(x>>>3));
        System.out.println("x >>> 1 " + xtemp);


        int i = 2048;
        for (int j = 0; j < 10; j++) {
            i >>= 1;
            System.out.println(i);
        }

    }

}
