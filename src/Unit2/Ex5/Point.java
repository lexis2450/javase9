package Unit2.Ex5;

/**
 * объект <code>класса Point</code>
 * описывает точку на плоскости
 * @author Alexandra Savelyeva
 * @version 1.0
 */
public class Point {
    double x;
    double y;

    public Point() {
        this.x = 0;
        this.y = 0;
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    /**
     * Изменение координаты точки на плоскости
     * @param distance_x изменение по оси х
     * @param distance_y изменение по оси у
     * @return новая переменная с измененной координатой
     */
    public Point translate (double distance_x, double distance_y){
        Point a = new Point();
        a.x = this.x + distance_x;
        a.y = this.y + distance_y;
        return a;
    }

    /**
     * Изменение масштаба
     * @param scl новый масштаб
     * @return координаты в новом масштабе
     */
    public Point scale(double scl){
        Point a = new Point();
        a.x = this.x * scl;
        a.y = this.y * scl;
        return a;
    }

    public static void main(String[] args) {
        Point p = new Point(3, 4).translate(1, 3).scale(0.5);
        System.out.println(p.x + "   "+ p.y);
    }
}
