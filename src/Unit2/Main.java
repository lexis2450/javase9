package Unit2;

import java.util.Arrays;

class MyClass{
    double a;

    public MyClass(double a) {
        this.a = a;
    }

    @Override
    public String toString() {
        return "MyClass{" +
                "a=" + a +
                '}';
    }
}

public class Main {


    public static void metod(int[] a){
        for (int i = 0; i < a.length; i++) {
            a[i]=a[i]+1;
        }
    }

    public static void metod1(MyClass a){
        a.a += 1;
    }

    public static void main(String[] args) {
        int[] a = {1, 2, 3, 4, 5};
        metod(a);
        System.out.println(Arrays.toString(a));
        String str = "lol";
        MyClass b = new MyClass(12);
        metod1(b);
        System.out.println(b.toString());
    }
}
