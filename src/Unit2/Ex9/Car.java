package Unit2.Ex9;


public class Car {
    private double currentDistance;
    private double currentGas;
    private double kpd;

    final double MAX_VALUE = 40.0;

    public Car(double kpd) {
        this.currentGas = 40;
        this.currentDistance = 0;
        this.kpd = kpd;
    }

    @Override
    public String toString() {
        return "Car{" +
                "currentDistance=" + currentDistance +
                ", currentGas=" + currentGas +
                ", kpd=" + kpd +
                ", MAX_VALUE=" + MAX_VALUE +
                '}';
    }

    public void shiftDistance(double a){
        double possibleChange = this.currentGas*this.kpd;
        if(possibleChange<a){
            System.out.println("Бензина в машине только на "+ possibleChange+ " км");
        } else {
            this.currentDistance += a;
            this.currentGas -= a;
        }
    }

    public void shiftGas(double value){
        double gas = this.currentGas + value;
        if (gas > MAX_VALUE){
            System.out.println("Можно не больше" + (MAX_VALUE-this.currentGas));
        } else{
            this.currentGas += value;
        }
    }

    public double getCurrentDistance() {
        return currentDistance;
    }

    public double getCurrentGas() {
        return currentGas;
    }

    public static void main(String[] args) {
        Car a = new Car(0.8);
        System.out.println(a.toString());
        a.shiftDistance(32);
        System.out.println(a.toString());
        a.shiftDistance(20);
        a.shiftGas(30);
        System.out.println(a.toString());


    }


}
