package Unit2.Ex1;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class MainClass {
    //enum MyWeekday { SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THUTHDAY, FRIDAY, SATURDAY };


    public static void main(String[] args) {
        int year = 2018;
        int month = 12;
        LocalDate date = LocalDate.of(year, month, 1);
        System.out.println("Sun Mon Tue Wed Thu Fri Sat");
        DayOfWeek weekday = date.getDayOfWeek();
        int value = (weekday.plus(1).getValue());
        int value1 = (weekday.ordinal());
        for (int i = 1; i < value; i++)
            System.out.print("    ");
        while (date.getMonthValue() == month) {
            System.out.printf("%4d", date.getDayOfMonth());
            date = date.plusDays(1);
            if (date.plusDays(1).getDayOfWeek().getValue() == 1)
                System.out.println();
        }
        if (date.plusDays(1).getDayOfWeek().getValue() != 1)
            System.out.println();

    }
}
