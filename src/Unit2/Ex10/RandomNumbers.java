package Unit2.Ex10;

import java.util.ArrayList;
import java.util.Random;

public class RandomNumbers {
    private static Random generator = new Random();

    public static int RandomElement (int[] a){
        if(a.length!=0) {
            int i = generator.nextInt(a.length);
            return a[i];
        } else {
            return 0;
        }
    }

    public static int RandomElement (ArrayList<Integer> a){
        if(!a.isEmpty()) {
            int i = generator.nextInt(a.size());
            return a.get(i);
        } else {
            return 0;
        }
    }



    public static int nextInt(int low, int high) {
        return low + generator.nextInt(high - low + 1);
        // Ok to access the static generator variable
    }

    public static void main(String[] args) {
        int[] a = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        //a1()

    }
}
