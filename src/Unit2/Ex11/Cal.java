package Unit2.Ex11;

import java.time.DayOfWeek;
import java.time.LocalDate;
import static java.lang.System.*;
import static java.time.LocalDate.*;
import static java.io.PrintStream.*;

public class Cal {
    //enum MyWeekday { SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THUTHDAY, FRIDAY, SATURDAY };


    public static void main(String[] args) {
        int year = 2018;
        int month = 12;
        LocalDate date = of(year, month, 1);
        out.println("Sun Mon Tue Wed Thu Fri Sat");
        DayOfWeek weekday = date.getDayOfWeek();
        int value = (weekday.plus(1).getValue());
        int value1 = (weekday.ordinal());
        for (int i = 1; i < value; i++)
            System.out.print("    ");
        while (date.getMonthValue() == month) {
            System.out.printf("%4d", date.getDayOfMonth());
            date = date.plusDays(1);
            if (date.plusDays(1).getDayOfWeek().getValue() == 1)
                System.out.println();
        }
        if (date.plusDays(1).getDayOfWeek().getValue() != 1)
            System.out.println();

    }
}