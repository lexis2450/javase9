package Unit2.Ex6;

/**
 * объект <code>класса Point</code>
 * описывает точку на плоскости
 * @author Alexandra Savelyeva
 * @version 1.0
 */
public class Point {
    double x;
    double y;

    public Point() {
        this.x = 0;
        this.y = 0;
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    /**
     * Изменяет координату точки
     * @param distance_x изменение координаты на оси x
     * @param distance_y изменение координаты по оси y
     */
    public void translate (double distance_x, double distance_y){
        this.x = this.x + distance_x;
        this.y = this.y + distance_y;
    }

    /**
     * Изменение масштаба
     * @param scl новый масштаб
     */
    public void scale(double scl){
        this.x = this.x * scl;
        this.y = this.y * scl;
    }

    public static void main(String[] args) {
        Point p = new Point(3, 4);
        p.translate(1, 1);
        System.out.println(p.toString());
        p.scale(0.5);
        System.out.println(p.toString());
    }
}
