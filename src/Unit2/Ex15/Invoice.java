package Unit2.Ex15;
import java.util.ArrayList;


public class Invoice {

    private static class Item {
        String description;
        int quantity;
        double unitPrice;

        public Item(String description, int quantity, double unitPrice) {
            this.description = description;
            this.quantity = quantity;
            this.unitPrice = unitPrice;
        }

        double price() {
            return quantity * unitPrice;
        }

        public String toString() {
            return quantity + " x " + description + " //// $" + unitPrice + " each";
        }
    }

    private ArrayList<Item> items = new ArrayList<>();

    public void addItem(String description, int quantity, double unitPrice){
            Item element = new Item(description, quantity, unitPrice);
            items.add(element);
        }

        public void print(){
            double total = 0;
            for (Item item:items) {
                System.out.println(item);
                total += item.price();
            }
            System.out.println("_________________________");
            System.out.println(total);
        }

        public static void main(String[] args) {
            Invoice a = new Invoice();
            a.addItem("plot", 4, 20);
            a. addItem("stone", 5, 10);
            a.print();


        }
}

