package Unit2.Ex16;

import java.util.Iterator;

public class Queue {


    public class MyIterator implements Iterator<Node>{
        private Node node;

        public MyIterator(Node node) {
            this.node = node;
        }

        @Override
        public boolean hasNext() {
            if(node.next!= null){
                return true;
            }else{
            return false;
            }
        }

        @Override
        public Node next(){
            if(this.hasNext()) {
                Node e = node.next;
                node = node.next;
                return e;
            }else{
                return null;
            }
        }

        public Node curr(){
            return this.node;
        }
    }

    private class Node{
        String value;
        Node next;
        Node prev;

        public Node(String value, Node next, Node prev) {
            this.value = value;
            this.next = next;
            this.prev = prev;
        }

        public Node(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    Node head;
    Node tail;
    private int length=0;


    public void addElement(String str){
        if(head==null){
            head = new Node(str, null, null);
            tail = head;
            head.next = tail;
        }else{
            Node newEntry = new Node(str, null, tail);
            tail.next = newEntry;
            tail = newEntry;

        }
        length++;
    }

    public void remove(){

        head = head.next;
        head.prev = null;
        length--;
    }

    public Iterator<Node> getI(){
        Iterator a = new MyIterator(head);
        return a;
    }

    public static void main(String[] args) {
        Queue arr = new Queue();
        arr.addElement("Саша");
        arr.addElement("Тая");
        arr.addElement("Сережа");
        arr.remove();
        Iterator<Node> i = arr.getI();
        //System.out.println(i.);
        System.out.println(i.next().getValue());
        System.out.println(i.next());
        //System.out.println(i.next().getValue());
        //System.out.println(i.next().getValue());



    }

}
