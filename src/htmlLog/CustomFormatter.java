package htmlLog;

public class CustomFormatter extends java.util.logging.Formatter {

    @Override
    public String format(java.util.logging.LogRecord record) {
        StringBuffer sb = new StringBuffer();
        sb.append("<p>");
        sb.append(record.getLevel());
        sb.append(" ");
        sb.append(formatMessage(record));
        sb.append("</p>");
        return sb.toString();
    }

    @Override
    public String getHead(java.util.logging.Handler h) {
        return "<HTML><HEAD></HEAD><BODY>";
    }

    @Override
    public String getTail(java.util.logging.Handler h) {
        return "</BODY></HTML>";
    }

}
