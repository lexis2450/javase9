package htmlLog;

public class Main {

    public static void main(String[] args) throws java.io.IOException {
        java.util.logging.Logger logger = java.util.logging.Logger.getLogger(Main.class.getName());
        java.util.logging.FileHandler fileHandler = new java.util.logging.FileHandler("log.html");
        fileHandler.setFormatter(new CustomFormatter());
        logger.addHandler(fileHandler);
        logger.info("log1");
        logger.info("log2");
        logger.info("log3");
    }
}
