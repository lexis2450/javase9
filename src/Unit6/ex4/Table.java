package Unit6.ex4;

import java.util.ArrayList;

public class Table<K, V> {
    public static class Entry<Integer, String> {
        private Integer key;
        private java.lang.String value;

        public Entry(Integer key, java.lang.String value) {
            this.key = key;
            this.value = value;
        }

        public Integer getKey() {
            return key;
        }

        public java.lang.String getValue() {
            return value;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public void setValue(java.lang.String value) {
            this.value = value;
        }
    }

    ArrayList<Entry<Integer, String>> myMap;

    public Table(int size) {
        this.myMap = new ArrayList<Entry<Integer, String>>();
    }

    public V getValue(K key){
        for (Entry entry : myMap){
            if (entry.getKey() == key){
                return (V)entry.getValue();
            }
        }
        return null;
    }

    public void setValue(Integer key, String value){
        for(Entry entry : myMap) {
            if (entry.getKey() == key) {
                entry.setValue(value);
            }
        }
    }

    public void remove(K key){
        for(Entry entry : myMap) {
            if (entry.getKey().equals(key)) {
                myMap.remove(key);
            }
        }
    }

    public static void main(String[] args) {
        Integer one = new Integer(1);
        Integer one1 = new Integer(11);
        Entry<Integer, String> a = new Entry<Integer, String>(one, "Sasha");
        Entry<Integer, String> b = new Entry<Integer, String>(one1, "Sasha1");
        Table<Integer, String> myTable = new Table<Integer, String>(2);
        myTable.myMap.add(a);
        myTable.myMap.add(b);
    }
    //Класс м б необобщенным

}