package Unit6.ex5;

import java.lang.reflect.Array;

public class MyClass<T> {

    public MyClass() {
    }

    public static <T> T[] swap(int i, int j, T... values){
        //T[] tempArr = (T[]) Array.newInstance(values.getClass().getComponentType(), values.length);
        T temp = values[i];
        values[i] = values[j];
        values[j] = temp;
        return values;
    }

    public static void main(String[] args) {
        //Double[] result = swap(0, 1, 1.5, 2, 3);
        Double[] result1 = (Double[]) MyClass.<Double>swap(0, 1, 1.5, 2.0, 3.0);
    }
    //добавила нолики


}
