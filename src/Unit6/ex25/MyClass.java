package Unit6.ex25;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.StringJoiner;

public class MyClass {
    public static String genericDeclaration(Method m) {
        StringBuilder sb = new StringBuilder();
        int modifiers = m.getModifiers();
        sb.append(Modifier.toString(modifiers));
        TypeVariable<Method>[] types = m.getTypeParameters();
        sb.append(" <");
        for (TypeVariable<Method> t : types) {
            sb.append(t.getName());
        }
        sb.append("> ");

        Type returnType = m.getGenericReturnType();
        sb.append(returnType.getTypeName()).append(" ");
        sb.append(m.getName());
        sb.append("(");


        Type[] paramTypes = m.getGenericParameterTypes();
        for (Type t : paramTypes) {
            sb.append(t.getTypeName());
        }

        sb.append(")");
        return sb.toString();
    }

    public static void main(String[] args) throws NoSuchMethodException {
        Method method = String.class.getMethod("isEmpty");
        Method method1 = Method.class.getMethod("getDeclaringClass");
        String a  = genericDeclaration(method1);
        System.out.println(a);
    }
}
