package Unit6.ex1;

import javax.swing.text.html.parser.Entity;
import java.util.ArrayList;

public class Stack<E> {
    public ArrayList<E> myArray;

    public Stack() {
        this.myArray = new ArrayList<>();
    }

    public void push(E ent){
        myArray.add(ent);
    }

    public void pop(){
        myArray.remove(myArray.size()-1);
    }

    public boolean isEmpty(int index){
        if(myArray.get(index) == null){
            return true;
        }
        else{
            return false;
        }
    }

    public static void main(String[] args) {
        Stack<String> str = new Stack<>();
        String ent = null;
        str.push("anton");
        str.push(null);
        str.push("maksim");
        str.push("sasha");
        System.out.println(str.isEmpty(1));
        str.pop();
    }
}
