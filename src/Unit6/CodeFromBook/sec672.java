package Unit6.CodeFromBook;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class sec672 {
    public static void main(String[] args) throws NoSuchMethodException {
        TypeVariable<Class<ArrayList>>[] vars = ArrayList.class.getTypeParameters();
        String name = vars[0].getName();
        Method m = Collections.class.getMethod("sort", List.class);
        TypeVariable<Method>[] vars1 = m.getTypeParameters();
        String name1 = vars[0].getName();
        Type[] bounds = vars[0].getBounds();
        if (bounds[0] instanceof ParameterizedType) {
            ParameterizedType p = (ParameterizedType) bounds[0];
            Type[] typeArguments = p.getActualTypeArguments();
            if (typeArguments[0] instanceof WildcardType) {
                WildcardType t = (WildcardType) typeArguments[0];
                Type[] upper = t.getUpperBounds();
                Type[] lower = t.getLowerBounds();
                if (lower.length > 0) {
                    String description = lower[0].getTypeName();
                }
            }
        }
    }
}
