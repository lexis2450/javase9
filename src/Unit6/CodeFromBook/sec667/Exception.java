package Unit6.CodeFromBook.sec667;

import Unit6.CodeFromBook.Exceptions;

import java.util.concurrent.Callable;

public class Exception {
    @SuppressWarnings("unchecked")
    private static <T extends Throwable>
    void throwAs(Throwable e) throws T {
        throw (T) e; // The cast is erased to (Throwable) e
    }

    public static <V> V doWork(Callable<V> c) {
        try {
            return c.call();
        } catch (Throwable ex) {
            //Exceptions.<RuntimeException>throwAs(ex);
            return null;
        }
    }
}
