package Unit6.CodeFromBook;

import java.util.ArrayList;

public class sec63 {
    public static <T extends AutoCloseable & Runnable> void closeAll(ArrayList<T> elems) throws Exception {
        for(T elem : elems) elem.close();
    }
}
