package Unit6.CodeFromBook;

public class Arrays {
    public static <T> void swap(T[] array, int i, int j){
        T temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    public static void main(String[] args) {
        String[] friends = {"Olya", "Sveta", "Katya"};
        System.out.println(java.util.Arrays.deepToString(friends));
        Arrays.swap(friends, 0, 1);
        System.out.println(java.util.Arrays.deepToString(friends));

    }
}
