package Unit6.CodeFromBook;

import java.util.ArrayList;

public class sec644 {
    public static boolean hasNulls(ArrayList<?> elements){
        for(Object e : elements){
            if(e == null) return true;
        }
        return false;
    }

    public static void main(String[] args) {
        ArrayList<String> a = new ArrayList<>();
        a.add("Sasha");
        a.add(null);
        boolean t = hasNulls(a);
        System.out.println(t);
    }
}
