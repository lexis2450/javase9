package Unit6.CodeFromBook;

import java.util.ArrayList;

public class sec671 {
    public static <T> ArrayList<T> repeat(int n, Class<T> cl)
            throws ReflectiveOperationException {
        ArrayList<T> result = new ArrayList<>();
        for (int i = 0; i < n; i++)
            result.add(cl.getConstructor().newInstance());
        return result;
    }

    public static void main(String[] args) {
        /*Class<? super T> getSuperclass()
                <U> Class<? extends U> asSubclass(Class<U> clazz)
        T cast(Object obj)
        Constructor<T> getDeclaredConstructor(Class<?>... parameterTypes)
        T[] getEnumConstants();*/
    }

}
