package Unit6.CodeFromBook;

import java.util.ArrayList;

public class sec645 {


    private static <T> void swapHelper(ArrayList<T> elements, int i, int j) {
        T temp = (T) elements.get(i);
        elements.set(i, elements.get(j));
        elements.set(j, temp);
    }
    public static void swap (ArrayList < ? > elements,int i, int j){
        swapHelper(elements, i, j);
    }
}
