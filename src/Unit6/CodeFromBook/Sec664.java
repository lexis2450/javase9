package Unit6.CodeFromBook;



import java.util.ArrayList;

public class Sec664<T> {
    public static <T> ArrayList<T> asList (T... elements){
        ArrayList<T> result = new ArrayList<>();
        for(T e : elements){
            result.add(e);
        }
        return result;
    }

    public static void main(String[] args) {
        //sec61.Entry<String, Integer> entry1 = new sec61.Entry<String, Integer>();
        //ArrayList<sec61.Entry<String, Integer>> entries = Sec664.asList(entry1);

        //@SuppressWarnings("unchecked")
        //Entry<String, Integer>[] entries;
        //entries = (Entry<String, Integer>) new Entry<?,?>[100];
        //ArrayList<Entry<String, Integer>> entries1 = new ArrayList<>(100);

    }
}
