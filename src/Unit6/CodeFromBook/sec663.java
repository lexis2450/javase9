package Unit6.CodeFromBook;

import java.util.function.IntFunction;

public class sec663 {
    public static <T> T[] repeat(int n, T obj, IntFunction<T[]> constr){
        T[] result = constr.apply(n);
        for(int i = 0; i < n; i++) result[i] = obj;
        return result;
    }

    public static <T> T[] repeat1(int n, T obj, Class<T> cl){
        @SuppressWarnings("unchecked") T[] result = (T[]) java.lang.reflect.Array.newInstance(cl, n);
        for(int i = 0; i < n; i++) result[i] = obj;
        return result;
    }

    public static void main(String[] args) {
        String[] greeting = sec663.repeat(10, "Hi", String[]::new);
        String[] greeting1 = sec663.repeat1(10, "Hi", String.class);
    }

    public static <T> T[] repeat (int n, T obj, T[] array){
        T[] result;
        if(array.length >= n)
            result = array;
        else{
            @SuppressWarnings("unchecked") T[] newArray =
                    (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), n);
            result = newArray;
        }
        for (int i = 0; i < n; i++) result[i] = obj;
        return result;
    }
}
