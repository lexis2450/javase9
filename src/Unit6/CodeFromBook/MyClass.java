package Unit6.CodeFromBook;

import java.util.ArrayList;
import java.util.Objects;

public class MyClass {
    public static boolean hasNulls(ArrayList<?> elements) {
        for(Object e : elements) {
            if(e == null){
                return true;
            }
        }
        return false;
    }
}
