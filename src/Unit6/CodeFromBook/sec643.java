package Unit6.CodeFromBook;

import Unit3.Ex15.Employee;
import Unit6.Predicate;

import java.util.Collections;

public class sec643 {
    public static<T> void printAll(T[] elements, Predicate<? super T> filter){
        for(T e : elements)
            if(filter.test(e))
                System.out.println(e.toString());
    }
}

interface Collection<E>{
    public void addAll(Collection<? extends E> c);

}
