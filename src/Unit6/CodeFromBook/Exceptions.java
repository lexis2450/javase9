package Unit6.CodeFromBook;

import java.util.concurrent.Callable;

public class Exceptions {
    /*public static <V, T> V doWork(Callable<V> c, T ex)throws T{
        try{
            return c.call();
        }catch (Throwable realEx){
            ex.initCause(realEx);
            throw ex;
        }
    }*/

    @SuppressWarnings("unchecked")
    private static <T extends Throwable> void throwAs (Throwable e) throws T {
        throw (T) e;
    }

    public static <V> V doWork(Callable<V> c){
        try{
            return c.call();
        }catch (Throwable ex) {
            Exceptions.<RuntimeException> throwAs(ex);
            return null;
        }
    }

}
