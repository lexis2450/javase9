package Unit6.ex8;

public class Pair<E extends Comparable<E>> {
    private E first;
    private E second;

    public Pair(E first, E second) {
        this.first = first;
        this.second = second;
    }

    public E getFirst() {
        return first;
    }

    public E getSecond() {
        return second;
    }

    public void setFirst(E first) {
        this.first = first;
    }

    public void setSecond(E second) {
        this.second = second;
    }

    public E max() {
        return this.first.compareTo(this.second) > 0 ? this.first : this.second;
    }

    public E min(){
        return this.first.compareTo(this.second) > 0 ? this.first : this.second;
    }

    public static
    void main(String[] args) {
       // Pair<Tel> telPair = new Pair<Tel>();
    }
}

