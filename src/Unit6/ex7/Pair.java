package Unit6.ex7;

public class Pair<E> {
    private E fst;
    private E scnd;

    public Pair(E fst, E scnd) {
        this.fst = fst;
        this.scnd = scnd;
    }

    public Pair() {
    }

    public E getFst() {
        return fst;
    }

    public E getScnd() {
        return scnd;
    }

    public void setFst(E fst) {
        this.fst = fst;
    }

    public void setScnd(E scnd) {
        this.scnd = scnd;
    }
}
