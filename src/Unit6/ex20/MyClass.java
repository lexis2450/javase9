package Unit6.ex20;

import java.util.ArrayList;

public class MyClass {
    @SafeVarargs
    public static final <T> T[] repeat(int n, T... obj){
        int k = obj.length;
        @SuppressWarnings("unchecked")
        T[] results = (T[])java.lang.reflect.Array.newInstance(obj.getClass().getComponentType(), n * obj.length);
        for (int i = 0; i < n; i++) {
            System.arraycopy(obj, 0, results, i * obj.length, obj.length);
        }
        return results;
    }

    public static void main(String[] args) {
        ArrayList<Integer> arr = new ArrayList<>();
        arr.add(1);
        arr.add(2);
        arr.add(3);
        Integer a1 = 10;
        Integer a2 = 20;
        Integer a3 = 30;
        Integer[] a = repeat(3, a1, a2, a3);


    }
}
