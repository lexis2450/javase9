package Unit6.ex14;

import Unit5.Ex9.Element;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class MainClass {
    public static <T extends AutoCloseable> void closeAll(List<T> elems) throws Exception {
        int count = 0;
        Exception ex = null;
        for (T elem : elems) {
            try {
                elem.close();
            } catch (Exception e) {
                //ex = new Exception(e.getMessage(), ex);
                if(ex == null){
                    ex = e;
                } else {
                    ex.addSuppressed(e);
                }
                count++;
            }
        }
        System.out.println(count);
        if (ex != null) {
            throw ex;
        }
    }

    public static
    void main(String[] args) throws Exception {
        List<Cl> a = new ArrayList<>();
        Collections.addAll(a, new Cl(), new Cl(), new Cl(), new Cl(), new Cl(), new Cl());
        closeAll(a);

    }
}
