package Unit6;

public class sec61 {
    public class Entry<K, V>{
        private K key;
        private V value;

        public Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }
    }

    public static void main(String[] args) {
        //Entry<String, Integer> ntr = new Entry<>("lol", 12);
    }
}
