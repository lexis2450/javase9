package Unit6.ex6;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainClass<E> {
    public ArrayList<? extends E> metod(ArrayList<E> arr1, ArrayList<E> arr2){
        int length1 = arr1.size();
        int length2 = arr2.size();
        for (int i = 0; i < length2; i++) {
            arr1.add(arr2.get(i));
        }
        return arr1;
    }

    public ArrayList<? super E> metod1(ArrayList<E> arr1, ArrayList<E> arr2){
        arr1.addAll(arr2);
        return arr1;
    }

    //как создать новый лист внутри метода,\???
    public static void main(String[] args) {
        MainClass mk = new MainClass();
        ArrayList<String> a = new ArrayList<>();
        a.add("Hello1");
        a.add("Hello2");
        a.add("Hello3");
        a.add("Hello4");
        a.add("Hello5");
        a.add("Hello6");
        ArrayList<String> b = new ArrayList<>();
        b.add("Hi");
        ArrayList<String> c = (ArrayList<String>) mk.metod(a, b);
        ArrayList<String> d = (ArrayList<String>) mk.metod1(a, b);
        ArrayList<Object> s = new ArrayList<>();
        s.add(88);
        ArrayList<String> arrayList = mk.metod1(a, s);

    }
}
