package Unit6;

import Unit3.Ex1.Employee;

public class MyClass {
    public static void printAll(Employee[] staff, Predicate<Employee> filter){
        for(Employee e : staff){
            if(filter.test(e)){
                System.out.println(e.name);
            }
        }
    }

    public static void printAll1(Employee[] staff, Predicate<? super Employee> filter){
        for(Employee e : staff){
            if(filter.test(e)){
                System.out.println(e.name);
            }
        }
    }

    public static void main(String[] args) {
        Employee[] employees = new Employee[2];
        printAll(employees, e -> e.getMeasure() > 100);
        Predicate<Object> eventLength = e -> e.toString().length() % 2 == 0;
        //printAll(employees, eventLength); ОШИБКА
        printAll1(employees, eventLength);
    }
}
