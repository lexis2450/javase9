package Unit6.ex12;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainClass {
    public static <T> void minmax(List<T> elements, Comparator<? super T> comp, List<? super T> result) {
        result.add(Collections.min(elements, comp));
        result.add(Collections.max(elements, comp));
    }

    public static
    void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(11);
        a.add(10);
        a.add(5);
        ArrayList<Integer> v = new ArrayList<>();
        //v.add(0)
        minmax(a, (t1, t2) -> t1.compareTo(t2), v);
    }
}
