package Unit6.ex9;

import Unit6.ex7.Pair;

import java.util.ArrayList;
import java.util.Collections;

public class MainClass<E> {
    public static <E> Pair<E> firstLast(ArrayList<? extends E> a) {
        Pair<E> p = new Pair<E>();
        p.setFst(a.get(0));
        p.setScnd(a.get(a.size() - 1));
        return p;
    }

    public static <E extends Comparable<E>> E min(ArrayList<E> a) {
        return Collections.min(a);
    }

    public static <E extends Comparable<E>> E max(ArrayList<E> a){
        return Collections.max(a);
    }

    public static <E extends Comparable<E>> Pair<E> minMax(ArrayList<E> a) {
        Pair<E> p = new Pair<E>();
        p.setFst(min(a));
        p.setScnd(max(a));
        return p;
    }

    public static
    void main(String[] args) {
        ArrayList<Integer> a = new ArrayList();
        a.add(11);
        a.add(10);
        a.add(5);
        Pair v = firstLast(a);
        //System.out.println();

    }
}
