package Unit6;

public interface Predicate<T> {
    public boolean test(T arg);
}
