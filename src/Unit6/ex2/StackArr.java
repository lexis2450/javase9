package Unit6.ex2;

import java.lang.reflect.Array;

public class StackArr <E> {
    private E[] myStack;

    public StackArr() {
        //this.myStack = (E[]) Array.newInstance(myStack.getClass().getComponentType(),  initialSize);
        myStack = (E[]) new Object[0];
    }

    public void setMyStack(E[] myStack) {
        this.myStack = myStack;
    }

    public void push(E ent){
        E[] es = (E[]) Array.newInstance(myStack.getClass().getComponentType(), myStack.length + 1);
        for (int i = 0; i < myStack.length; i++) {
            es[i] = myStack[i];
        }
        es[myStack.length] = ent;
        myStack = es;
    }

    public void pop(){
        E[] es = (E[]) Array.newInstance(myStack.getClass().getComponentType(), myStack.length - 1);
        for (int i = 0; i < myStack.length - 1; i++) {
            es[i] = this.myStack[i];
        }
        myStack = es;
    }

    public boolean isEmpty(int index){
        if(myStack[index] == null) return true;
        else return false;
    }

    public static void main(String[] args) {
        StackArr<String> objectStackArr = new StackArr<>();
        objectStackArr.push("Sergey");
        objectStackArr.push("Anton");
        objectStackArr.push("Maksim");

    }
}
