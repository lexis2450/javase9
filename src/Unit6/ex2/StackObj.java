package Unit6.ex2;

import java.lang.reflect.Array;

public class StackObj<E> {
    public Object[] myStack;

    public StackObj() {
        this.myStack = new Object[0];
    }

    public void push(E ent){
        E[] es = (E[]) Array.newInstance(myStack.getClass().getComponentType(), myStack.length + 1);
        for (int i = 0; i < myStack.length; i++) {
            es[i] = (E)this.myStack[i];
        }
        es[myStack.length] = ent;
        this.myStack = (E[]) es;
    }

    public void pop(){
        E[] es = (E[]) Array.newInstance(myStack.getClass().getComponentType(), myStack.length - 1);
        for (int i = 0; i < myStack.length - 1; i++) {
            es[i] = (E)this.myStack[i];
        }
        this.myStack = (E[]) es;
    }

    public boolean isEmpty(int index){
        if(myStack[index] == null) return true;
        else return false;
    }

    public static void main(String[] args) {
        StackObj<String> str = new StackObj<>();
        str.push("Str1");
        str.push(null);
        str.push("Str2");
        str.push("Str3");
        str.pop();
        System.out.println(str.isEmpty(1));
    }
}
