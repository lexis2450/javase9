package Unit6.ex22;

import javax.security.auth.callback.Callback;
import java.util.concurrent.Callable;
import java.util.function.Function;

public class MyClass {
    public static <V, T extends Throwable> V doWork(Callable<V> c, Function<Throwable, T> constructor) throws T {
        try {
            return c.call();
        } catch (Throwable realEx) {
            throw constructor.apply(realEx);
        }
    }

    public static <V> V doWork(Callable<V> c) {
        try {
            return c.call();
        } catch (Throwable realEx) {
            //throw (RuntimeException)realEx;
            MyClass.<RuntimeException>throwAs(realEx);
        }
        return null;
    }

    private static <T extends Throwable> void throwAs(Throwable e) throws T{
        throw (T) e;
    }

    public static void main(String[] args) throws Throwable {
        MyClass a = new MyClass();
        doWork(() -> {throw new Exception();});

        //doWork()

    }
}
