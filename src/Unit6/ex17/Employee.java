package Unit6.ex17;

import java.util.stream.Stream;

public class Employee implements Comparable<Employee> {
    private String name;
    private double salary;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public int compareTo(Employee o) {
        return Double.compare(this.salary, o.salary);
    }

    public static void main(String[] args) {
        Employee empl = new Employee();
        Employee emp2 = new Employee();
        empl.setName("Slava");
        empl.setSalary(1000);
        emp2.setName("Nikita");
        emp2.setSalary(2000);
        System.out.println(empl.compareTo(emp2));
        Stream.of(Employee.class.getDeclaredMethods()).forEach(System.out::println);

    }
}
