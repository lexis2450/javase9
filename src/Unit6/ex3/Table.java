package Unit6.ex3;

import Unit6.CodeFromBook.Entry;
import Unit6.sec61;

import java.util.ArrayList;

public class Table<K, V> {
    ArrayList<Entry<K, V>> myMap;

    public Table() {
        this.myMap = new ArrayList<>();
    }

    public V getValue(K key){
        for (Entry<K, V> entry : myMap){
            if (entry.getKey() == key){
                return entry.getValue();
            }
        }
        return null;
    }

    public void setValue(K key, V value){
        for(Entry entry : myMap) {
            if (entry.getKey() == key) {
                entry.setValue(value);
            }
        }
    }

    public void remove(K key){
        for(Entry entry : myMap) {
            if (entry.getKey().equals(key)) {
                myMap.remove(key);
            }
        }
    }

    public static void main(String[] args) {
        Integer one = new Integer(1);
        Integer one1 = new Integer(11);
        Entry<Integer, String> a = new Entry<Integer, String>(one, "Sasha");
        Entry<Integer, String> b = new Entry<Integer, String>(one1, "Sasha1");
        Table<Integer, String> myTable = new Table<Integer, String>();
        myTable.myMap.add(a);
        myTable.myMap.add(b);
    }

}
