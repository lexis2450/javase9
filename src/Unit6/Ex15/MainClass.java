package Unit6.Ex15;

import Unit6.ex7.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class MainClass {
    public static <R, T> List<R> map(List<T> list, Function<T, R> func) {
        List<R> result = new ArrayList<>();
        for (T e : list) {
            result.add(func.apply(e));
        }
        return result;
    }

    public static
    void main(String[] args) {
        List<Integer> a = new ArrayList<>();
        a.add(1);
        List<String> n = map(a, t -> {
           return t.toString();
        });
        System.out.println(n);
    }
}
