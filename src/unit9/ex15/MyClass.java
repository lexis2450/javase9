package unit9.ex15;

import unit9.ex13.Point;

import java.io.*;

public class MyClass {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Point[] arr = new Point[2];
        arr[0] = new Point(12, 10);
        arr[1] = new Point(100, 15);
        System.out.println(arr.toString());
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("arr.out"));
        objectOutputStream.writeObject(arr);
        objectOutputStream.close();
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("arr.out"));
        Object pointCp = objectInputStream.readObject();
        objectInputStream.close();
        System.out.println(pointCp.toString());
    }
}
