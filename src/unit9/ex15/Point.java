package unit9.ex15;

import java.io.*;

public class Point implements Serializable{
    double[] arr;

    public Point(double[] arr) {
        this.arr = arr;
    }

    public Point() {
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //unit9.ex13.Point[] arr = new unit9.ex13.Point[2];
        //arr[0] = new unit9.ex13.Point(12, 10);
        //arr[1] = new unit9.ex13.Point(100, 15);
        double[] numerucs = {1, 43};
        Point a = new Point(numerucs);
        System.out.println(a.toString());
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("arr.out"));
        objectOutputStream.writeObject(a);
        objectOutputStream.close();
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("arr.out"));
        Object pointCp = objectInputStream.readObject();
        objectInputStream.close();
        System.out.println(pointCp.toString());
    }
}
