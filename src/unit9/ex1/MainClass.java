package unit9.ex1;

import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.Files.newInputStream;


public class MainClass {
    public static void main(String[] args) throws IOException {
        Path pathR = Paths.get("C:\\Users\\alsa1118\\IdeaProjects\\javase9\\src\\unit9\\ex1\\textR");
        Path pathW = Paths.get("C:\\Users\\alsa1118\\IdeaProjects\\javase9\\src\\unit9\\ex1\\textW");
        String info = "I want to be forgiven";
        String msg;
        try(InputStream in = new ByteArrayInputStream(info.getBytes()); OutputStream out = new ByteArrayOutputStream()){
            in.transferTo(out);
            msg =  out.toString();
        }
        System.out.println(msg);

        FileInputStream in1 = new FileInputStream(new File(pathR.toString()));
        FileOutputStream out1 = new FileOutputStream(new File(pathW.toString()));
            FileChannel fhR = in1.getChannel();
            FileChannel fhW = out1.getChannel();
            fhW.transferFrom(fhR, 0, fhR.size());
            in1.close();
            out1.close();
    }
}
