package unit9.ex5;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.StandardCharsets;

public class MainClass {
    public static void main(String[] args) {
        Charset Charset1 = Charset.forName("UTF8");
        CharsetEncoder outEncoder = Charset1.newEncoder();
        System.out.println(outEncoder);
        System.out.println(outEncoder.getClass().getCanonicalName());
        System.out.println("CanonicalName ---- " + outEncoder.replacement().getClass().getCanonicalName());
        //outEncoder.
        System.out.println("-------------------------------");

        Charset Charset2 = Charset.forName("UTF16");
        CharsetEncoder outEncoder2 = Charset2.newEncoder();
        System.out.println(outEncoder2);
        System.out.println(outEncoder2.getClass().getCanonicalName());
        System.out.println("CanonicalName ---- " + outEncoder2.replacement().getClass().getCanonicalName());
        System.out.println("-------------------------------");

        Charset Charset3 = Charset.forName("UTF_16BE");
        CharsetEncoder outEncoder3 = Charset3.newEncoder();
        System.out.println(outEncoder3);
        System.out.println(outEncoder3.getClass().getCanonicalName());
        System.out.println("CanonicalName ---- " + outEncoder3.replacement().getClass().getCanonicalName());
        System.out.println("-------------------------------");

        Charset Charset4 = Charset.forName("UTF_16LE");
        CharsetEncoder outEncoder4 = Charset4.newEncoder();
        System.out.println(outEncoder4);
        System.out.println(outEncoder4.getClass().getCanonicalName());
        System.out.println("CanonicalName ---- " + outEncoder4.replacement().getClass().getCanonicalName());
        System.out.println("-------------------------------");

        Charset Charset5 = Charset.forName("ISO_8859_1");
        CharsetEncoder outEncoder5 = Charset5.newEncoder();
        System.out.println(outEncoder5);
        System.out.println(outEncoder5.getClass().getCanonicalName());
        System.out.println("CanonicalName ---- " + outEncoder5.replacement().getClass().getCanonicalName());
        System.out.println("-------------------------------");

        Charset Charset6 = Charset.forName("US-ASCII");
        CharsetEncoder outEncoder6 = Charset6.newEncoder();
        System.out.println(outEncoder6);
        System.out.println(outEncoder6.getClass().getCanonicalName());
        System.out.println("CanonicalName ---- " + outEncoder6.replacement().getClass().getCanonicalName());
        System.out.println("-------------------------------");

        byte[] arr = {1, 4 , 5, 6, -1, 3, 5, 90};
        String str1 = new String(arr, StandardCharsets.US_ASCII);
        System.out.println(str1);
        String str2 = new String(arr, StandardCharsets.UTF_8);
        System.out.println(str2);
        String str3 = new String(arr, StandardCharsets.ISO_8859_1);
        System.out.println(str3);
    }

}
