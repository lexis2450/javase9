package unit9.maks;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class copyFile {

    public static void main(String[] args) throws IOException {
        Path copied = Paths.get("C:\\Users\\alsa1118\\IdeaProjects\\javase9\\src\\unit9\\maks\\copiedWithNio.txt");
        Path originalPath = Paths.get("C:\\Users\\alsa1118\\IdeaProjects\\javase9\\src\\unit9\\maks\\text");
        Files.copy(originalPath, copied, StandardCopyOption.REPLACE_EXISTING);
    }

}
