package unit9.ex4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainClass {
    public static List<String> scannerReader(Path path) throws IOException {
        List<String> res = new ArrayList<>();
        int count = 0;
        try(Scanner in = new Scanner(path)){
            while(in.hasNext()){
                count++;
                res.add(in.nextLine());
            }
        }
        res.add(String.valueOf(count));
        return res;
    }

    public static List<String> bufferedReader(Path path) throws IOException {
        List<String> res = new ArrayList<>();
        int count = 0;
        try(BufferedReader reader =  new BufferedReader(
                new InputStreamReader(Files.newInputStream(path)))){
            Stream<String> lines = reader.lines();
            res = lines.collect(Collectors.toList());
            res.add(String.valueOf(res.size()));
        }
        return res;
    }

    public static List<String> bufferedReaderLine(Path path) throws IOException {
        List<String> res = new ArrayList<>();
        int count = 0;
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(Files.newInputStream(path)))){
            String line;
            while ((line = reader.readLine()) != null) {
                count++;
                res.add(line);
            }
        }
        return res;
    }

    public static void main(String[] args) throws IOException {
        Path path = Paths.get("C:\\Users\\alsa1118\\IdeaProjects\\javase9\\src\\unit9\\ex4\\test");
        List<String> a = scannerReader(path);
        System.out.println(a.toString());
        System.out.println("---------------------");
        List<String> b = scannerReader(path);
        System.out.println(b.toString());
        System.out.println("---------------------");
        List<String> c = scannerReader(path);
        System.out.println(c.toString());
    }
}
