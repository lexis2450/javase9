9 Организация ввода-вывода
====
###9.1. Потоки вводаб выводаб чтения и записи.
####9.1.1. Получение потоков ввода-вывода
см пример 91

####9.1.2. Ввод байтов
В классе InputStream имеется приведенный ниже метод для ввода одного байта
InputStream in = ...; 
int b = in.read();
Массовый ввод байтов
byte[] bytes = Files.readAllBytes();

int actualBytesRead = in.read(bytes); 
int actualBytesRead = in.readNBytes(bytes, start, length);

####9.1.3 Вывод байтов
Можно вывести один байт
int b = ...;
out.write(b);

Можно вывести массив байт
byte[] bytes = ...;
out.write(bytes);
out.write(bytes, start, length);

По окончании работы  с потоками обязательно закрой их
Или можно использовать try с ресурсами

Копируем поток ввода в поток вывода
try (InputStream in = ..., OutputStream out = ...) { 
    in.transferTo(out);
}
 Копируем данные из файла в поток
 Files.copy(path, out)
 Обратно
 Files(in, path, StandardCopyOption.REPLACE_EXISTING)
 
####9.1.4 Кодировки символов
потоки ввода вывода байтовые штуки
а инфу часто приходится получать в буквах
символы кодируются в байты
Юникод 21 разрядное целое число
UTF-8 каждый симол 1-4 байта (ASCII 1 байт)
UTF-16 кажд симол 1-2 16ти разрядных значения
Класс StandardCharsets, переменные типа CharSet(должны поддерживаться на каждой вирт машине)
StandardCharsets.UTF_8 
StandardCharsets.UTF_16 
StandardCharsets.UTF_16BE 
StandardCharsets.UTF_16LE 
StandardCharsets.ISO_8859_l 
StandardCharsets.US ASCII
Если хочешь превратить массив в строку
String str = new String(bytes, StandardCharacter.UTF-8);

####Ввод текста
класс Reader - поток чтения
класс-адаптер InputStreamReader
InputStream inStream = ...;
Reader in = new InputStreamReader(inStream, charset);

String content = new String(Files.readAllBytes(path), charset);
List<String> lines = Files.lines(path, charset)
Можно использовать потоки данных
try(Stream<String> lines = Files.lines(path, charset)){
  ...
  }
  
Сканнер
Scanner in = new Scanner(path, "UTF-8"); 
while (in.hasNextDouble()) { 
    double value = in.nextDouble();
}

Можно использовать обертки потоков(Если читаем не из файла)
try (BufferedReader reader =
    new BufferedReader(new InputStreamReader(url.openStream()))) {
        Stream<String> lines = reader.lines();
}

####9.1.6. Вывод текста
класс Writer
OutputStream outStream = ...;
Writer out = new OutputStreamWriter(outStream, charset); 
out.write(str);

Writer out = Files.newBufferedWriter(path, charset);

класс PrintWriter - удобный(есть всякие крутые методы println..)

PrintWriter out = new PrintWriter(outStream, "UTF-8");

Если текст подготовлен для вывода
Files.write(path, lines, charset);
lines - ссылка на интерфейс Collection<String> или Iterable<? extends CharSequence>


Чтобы дозаписать в файл
Files.write(path, content.getBytes(charset), StandardOpenOption.APPEND);
Files.write(path, lines, charset, StandardOpenOption.APPEND);

#### 9.1.7. Ввод-вывод двоичных данных
byte readByteO
int readUnsignedByte()
char readChar()
short readShortO
int readUnsignedShort()
int readlntO
long readLong()
float readFloatO
double readDoubleO
void readFully(byte[] b)
методы вводят и выводят числа с обратным порядком следования
удобно:
все числа занимают одного размера в памяти, ускоряется доступ к данным, 
не нужно проводить синтаксический анализ
минусы
нельзя посмтреть в текстовом редакторе
есть адаптеры-обертки
Datalnput in = new DatalnputStream(Files.newInputStream(path));
DataOutput out = new DataOutputStream(Files.newOutputStream(path) );

####9.1.8. Произвольный доступ к файлам
класс RandomAccessFile позволяет вводить и выводить данные а любом месте файла
реализует интерфейсы DataInput DataOutput
r - доступ для чтения 
rw - доступ для чтения и записи
RandomAccessFile file = new RandomAccessFile(path.toString(), "rw");
seek() - установить указатель в позицию
указываем целочисленное значение от 0 до длинны файла (length())
getFilePointer() узнать где указатель
int value = file.**readlnt()**;
file.seek(file.getFilePointer() - 4);
file.**writelnt**(value + 1);

####9.1.9 Файлы, отображаемые в памяти
Файлы, отображаемые в памяти, конечно, крутые.
Но если файл слишком большой
Тогда нужно сначала получить канал
FileChannel channel = FileChannel.open(path,
    StandardOpenOption.READ, StandardOpenOption.WRITE)
    
Затем отображаем в оперативную память
ByteBuffer buffer = channel.map(FileChannel.MapMode.READ_WRIТЕ,
    0, channel.size());
    
get(), getInt(), getDouble() / put()...аналог
по умолч. обратный порядок вывода байт
можно поменять 
buffer.order(ByteOrder.LITTLE_ENDIAN);

####9.1.10 Блокировка файлов
когда несколько программ изменяют один и тот же файл
помогает блокировка
класс FileChannel

FileChannel = FileChannel.open(path);
FileLock lock = channel.lock();

FileLock lock = channel.tryLock();

можно использовать в try с ресурсами
try (FileLock lock = channel.lock()) {
}

###9.2.Каталоги, файлы и пути к ним
интерфейс Path
путь, начинающийся с корневого каталога, называется абсолютным
иначе относительным
Path absolute = Paths.get("7”, "home", "cay");
Path relative = Paths.get("myapp", "conf", "user.properties");

Paths.get()
InvalidPathException

Path homeDirectory = Paths.get("/home/cay");
Объединение или разрешение путей
p.resolve(q)
если q - абсолютный путь, то результат q
иначе результат "p затем q"
normalize ()
toAbsolutePath ()

Path р = Paths.get("/home", "cay", "myapp.properties");
Path parent = p.getParent(); // Путь /home/сау
Path file = p. getFileName (); // Последний элемент myapp.properties
Path root = p.getRoot(); // Начальный отрезок пути / (или пустое
// значение null для относительного пути)
Path first = p.getName(0); // Первый элемент заданного пути 
Path dir = p.subpath(1, p.getNameCount());// Весь путь, кроме
// первого элемента cay/туарр.properties

####9.2.2 Создание файлов и каталогов
Files.createDirectories(path);
Files.createFile(path);
Files. exists (path)
isDirectory()
isRegularFile()
можно создавать временные файлы
Path tempFile = Files.createTempFile(dir, prefix, suffix);
...

####9.2.3 Копирование, перемещение и удаление файлов
Files.copy(fromPath, toPath);
Files.move(fromPath, toPath);
Files.copy(fromPath, toPath, StandardCopyOption.REPLACE_EXISTING,
StandardCopyOption.COPY_ATTRIBUTES);
если хотим перезаписать файл и сохр атрибуты
Files.move(fromPath, toPath, StandardCopyOption.ATOMIC_MOVE);
для атомарных операций
Files.delete(path);
можно удалять и директории
генерирует исключение, поэтому справедливо писать
boolean deleted = Files.deletelfExists(path);

####9.2.4. Обход элементов каталога
Files.list () возвращает поток данных типа Stream<Path>, 
откуда читаются элементы каталога
try с ресурсами
try (Stream<Path> entries = Files.list(pathToDirectory)) {
}
list не входит в подкаталоги
Files.walk() - обработает все порожденные эл-ты
try (Stream<Path> entries = Files.walk(pathToRoot)) {
// Содержит все порожденные элементы, обойденные в глубину
}
ограничить глубину дерева каталогов
Files. walk (pathToRoot, depth)
walkFileTree()

случаи, когда извещается метод обхода файлов.
1. Перед обработкой каталога:
FileVisitResult preVisitDirectory(Т dir, IOException ex)
2. Когда встречается файл или каталог:
FileVisitResult visitFile(T path, BasicFileAttributes attrs)
3. Когда в методе visit File () возникает исключение:
FileVisitResult visitFileFailed(Т path, IOException ex)
4. После обработки каталога:
FileVisitResult postVisitDirectory(T dir, IOException ex)

извещающий метод возвращает один из следующих результатов.
• Продолжить обход со следующего файла:
FileVisitResult.CONTINUE.
• Продолжить обход, но не обращаясь к элементам данного каталога:
FileVisitResult.SKIP_SUBTREE
• Прекратить обход:
FileVisitResult.TERMINATE.

####9.2.5 Системы файлов фомата ZIP
файловая система, содерж все зип файлы
FileSystem zipfs = FileSystems.newFileSystem(Paths.get(zipname), null);
Files.copy(zipfs.getPath(sourceName), targetPath);
перечислить все файлы в архиве зип
Files.walk(zipfs.getPath("/")).forEach(p -> {
Обработать узел дерева p });

###9.3. Подключения по заданному URL
#### 9.3.1 Классы URLConnection и HttpURLConnection
чтобы воспользоваться классом URLConnection необходимо

порядок действий.
1. Получить объект типа URLConnection:
URLConnection connection = uri.openConnection();
HttpURLConnection - аналогично

2. Задать, если требуется, свойства запроса:
connection.setRequestProperty("Accept-Charset", "UTF-8, ISO-8859-1");
(если ключу соответствует несколько значений, разделить их запятыми)

3. Сделать следующий вызов, чтобы отправить данные на сервер:
connection.setDoOutput(true);
try (OutputStream out = connection.getOutputStream()) {
    Вывести данные в поток вывода out
}
4. Если требуется прочитать заголовки из ответа, а метод getOutputStream () еще не вызывался, то сделать следующий вызов:
connection.connect();
Затем запросить данные из заголовков следующим образом:
Map<String, List<String>> headers = connection.getHeaderFields();
По каждому ключу получается список значений, поскольку одному и тому же ключу может соответствовать несколько полей в заголовках.
5. Прочитать ответ:
try (InputStream in = connection.getlnputstream()) {
    Ввести данные из потока ввода in
}

####9.3.2. Прикладной интерфейс API для HTTP клиентов
нововведение java9
более просой механизм для подключения к веб серверу
--add-modules jdk.incubator.httpclient
HTTP-клиентможет задавать вопросы и получать ответы от сервера
тип HTTPClient
HttpClient client =HTTPClient.newHTTPClient();
если требутся настроить клиента
HttpClient client =HTTPClient.newBuilder()
    .fillowRedirects(HTTPClient.Redirect.ALWAYS)
    .build();

Http Request request = HttpRequest.neBuilder()
    .uri(new URI("http: horstman.com"))
    .GET().build()

отправляем телозапроса в виде строки
Http Response<String> response = client.send(
    request, HttpResponse.BodyHandler.asString());

String bodyString = responce.body();

###9.4. Регулярныевыражения
Регулярные выражения обозначают шаблоны символьных строк

####9.4.1. Синтаксис регулярных выражений
. любой элемент
* предыдущие консрукции могут не повторяться
- повтореие 1и большераз
Классы символов
[0-9]
^ дополнение

####9.4.2. Обнаружение одного или всех совпадений
String regex = "[+-]?\\d+";
CharSequence input = ...;
if (Pattern.matches(regex, input)) {
.....
}

если много
Pattern pattern = Pattern.compile(regex);
Matcher matcher = pattern.matcher(input);
if (matcher.matches()) ...

если считываем с файла
Matcher matcher = pattern.matcher(input);
while (matcher.find()) {
    String match = matcher.group();
}

####9.4.3. Группы
применяются для извлечения составляющих совпадения
String contents = matcher.group(n);

Matcher matcher = pattern.matcher(input);
if (matcher.matches()) {
    item = matcher.group(1);
    currency = matcher.group(3);
    price = matcher.group(4);
}

item = matcher.group("item");
так можно извлечь имя

####9.4.4. Удаление и замена совпадений
Pattern commas = Pattern.compile("\\s*, \\s*");
    String[] tokens = commas.split(input);
    // Строка "1, 2, 3" превращается в строку ["1", "2", "3"]

Matcher matcher = commas.matcher(input);
String result = matcher.replaceAll(",");
//заменить все

####9.4.5. Признаки
признаки - флаги
как задавать
Pattern pattern = Pattern.compile(regex,
Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CHARACTER_CLASS);

###9.5. Сериализация
9.5.1. Интерфейс Serializable
public class Employee implements Serializable {
    private String name; private double salary;
}
сериализуем
ObjectOutputStream out = new ObjectOutputStream(Files.newOutputStream(path));
потом
Employee peter = new Employee (’’Peter", 90000);
Employee paul = new Manager("Paul", 180000); out.writeObject(peter); out.writeObject(paul);

диссериализация
ObjectInputStream in = new ObjectInputStream(Files.newInputStream(path));
Employee el = (Employee) in.readObject();
Employee e2 = (Employee) in.readObject();
серийный номер объекта

9.5.3. Методы readObject() и writeObject ()
private void readObject(ObjectlnputStream in) throws IOException, ClassNotFoundException
private void writeObject(ObjectOutputStream out) throws IOException

public class LabeledPoint implements Serializable {
    private String label;
    private transient Point2D point;
}

private void writeObject(ObjectOutputStream out) throws IOException {
    out.defaultWriteObj ect();
    out.writeDouble(point.getX());
    out.writeDouble(point.getY());
}

private void readObject(ObjectlnputStream in)
throws IOException, ClassNotFoundException {
in.defaultReadObj ect();
double x = in.readDouble();
double у = in.readDouble();
point = new Point2D(x, y);
}

