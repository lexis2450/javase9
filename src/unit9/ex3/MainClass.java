package unit9.ex3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainClass {
    public static void main(String[] args) throws IOException {
        String s;
        try (FileInputStream fis = new FileInputStream("C:\\Users\\alsa1118\\IdeaProjects\\javase9\\src\\unit9\\ex3"
                + "\\text"); InputStreamReader isr = new InputStreamReader(fis);) {
            s = isr.getEncoding();
            System.out.print("Character Encoding: " + s);
        }
        if(s == "UTF16"){
            int i = 1;
            String s1 = Integer.toBinaryString(i);
            char c = s1.toCharArray()[0];
            if (c == 1){
                System.out.println("прямой порядок");
            } else {
                System.out.println("обратный порядок");
            }
        }
    }
}


