package unit9.ex13;

import java.io.*;
import java.util.Arrays;

public class Point implements Serializable {
    double x;
    double y;

    Point(){

    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Point point = new Point(4, 12);
        byte[] bytes;
        try(ByteArrayOutputStream b = new ByteArrayOutputStream()){
            try(ObjectOutputStream o = new ObjectOutputStream(b)){
                o.writeObject(point);
            }
            bytes = b.toByteArray();
        }
        System.out.println(point.toString());
        System.out.println(Arrays.asList(bytes));

        Point o1;
        try(ByteArrayInputStream b = new ByteArrayInputStream(bytes)){
            try(ObjectInputStream o = new ObjectInputStream(b)){
                o1 = (Point) o.readObject();
            }
        }
        System.out.println(o1.toString());

    }

}
