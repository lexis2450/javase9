package unit9.ex8;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipOfFile {
    public static void main(String[] args) throws FileNotFoundException {
        String s = "C:\\Users\\alsa1118\\Desktop\\Sasha";
        File f = new File(s);
        if (!f.exists()) {
            System.out.println("\nNot found: " + s);
            System.exit(0);
        }

        if (!f.isDirectory()) {
            System.out.println(
                    "\nNot directory: " + s);
            System.exit(0);
        }
        ZipOutputStream zos = createZipOutputStream("!temp.zip");
        String[] sDirList = f.list();
        int i;
        try {
            for (i = 0; i < sDirList.length; i++) {
                File f1 = new File(
                        s + File.separator + sDirList[i]);

                if (f1.isFile()) { addFileToZip(zos, s + File.separator, sDirList[i]);
                }
            }
            zos.close();
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }

    static ZipOutputStream createZipOutputStream(String szPath) throws FileNotFoundException {
            File tempfile;
            ZipOutputStream zos = null;
            tempfile = new File(szPath);
            zos = new ZipOutputStream(new FileOutputStream(tempfile));
            zos.setLevel(Deflater.DEFAULT_COMPRESSION);
            return zos;
        }

    static void addFileToZip(ZipOutputStream zos, String szPath, String szName) throws Exception {
        System.out.print(szPath + szName);
        ZipEntry ze;
        ze = new ZipEntry(szName);
        zos.putNextEntry(ze);
        FileInputStream fis = new FileInputStream(szPath + szName);
        byte[] buf = new byte[8000];
        int nLength;
        while(true) {
            nLength = fis.read(buf);
            if(nLength < 0)
                break;
            zos.write(buf, 0, nLength);
        }
        fis.close();
        zos.closeEntry();
        long nSize = ze.getSize();
        long nCompressedSize =
                ze.getCompressedSize();

        long nPercent = 100 - ((nCompressedSize * 100) / nSize);

        System.out.println(" " + nSize + " (" + nCompressedSize + ") " + nPercent + "%");

    }

}
