package unit9.ex14;

import java.io.*;
import java.util.Arrays;

public class Point {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        unit9.ex13.Point point = new unit9.ex13.Point(4, 12);
        System.out.println(point.toString());
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("point.out"));
        objectOutputStream.writeObject(point);
        objectOutputStream.close();
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("point.out"));
        Object pointCp = objectInputStream.readObject();
        objectInputStream.close();
        System.out.println(pointCp.toString());
    }
}
