package unit9.ex2;

import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyClass {
    public static void main(String[] args) throws IOException {
        File file1 = new File("C:\\Users\\alsa1118\\IdeaProjects\\javase9\\src\\unit9\\ex2\\text");
        FileInputStream in = new FileInputStream(file1);

        //String info = new String(in.readAllBytes());
        String fileName = file1.getName();
        List<String> listInfo = Files.readAllLines(Paths.get(file1.getPath()), Charset.defaultCharset());
        int i = 1;
        Map<String, Integer> map = new HashMap<>();
        for(String str : listInfo){
            List<String> currStr = Arrays.asList(str.split("\\PL+"));
            for(String string : currStr){
                map.merge(string, 1, (x, y) -> x + y);
            }
            i++;
        }
        Path outPath = Files.createFile(Paths.get("C:\\Users\\alsa1118\\IdeaProjects\\javase9\\src\\unit9\\ex2\\" + fileName + ".toc"));
        FileOutputStream out = new FileOutputStream(outPath.toString());
        PrintWriter printFile = new PrintWriter(Files.newBufferedWriter(outPath, Charset.defaultCharset()));
        //printFile.println(map);
        for(Map.Entry<String, Integer> mapa : map.entrySet()){
            printFile.println(mapa);
        }
        in.close();
        out.close();
        printFile.close();





        System.out.println("File.separator = " + File.separator);
        System.out.println("File.separatorChar = " + File.separatorChar);
        System.out.println("File.pathSeparator = " + File.pathSeparator);
        System.out.println("File.pathSeparatorChar = " + File.pathSeparatorChar);
    }
}
