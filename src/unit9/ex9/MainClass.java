package unit9.ex9;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class MainClass {
    public static void main(String[] args) throws IOException {
        String username = "sasha";
        String password = "savelyeva";
        String input = username + ":" + password;
        String encoding = Base64.getEncoder().encodeToString(input.getBytes(StandardCharsets.UTF_8));
        URL myUrl = new URL("http://www.pro-java.ru");
        URLConnection myUrlCon = myUrl.openConnection();
        Object content = myUrlCon.getContent();
        System.out.println(content.toString());
        System.out.println(myUrlCon.getHeaderField(2));
        //System.out.println(myUrlCon.);
    }
}
