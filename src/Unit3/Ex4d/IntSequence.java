package Unit3.Ex4d;

public
interface IntSequence {

    boolean hasNext();

    int next();

    static
    IntSequence Of(int... values) {
        return new IntSequence() {
            int[] arr = values;
            int arrLength = arr.length;
            int i = 0;

            @Override
            public
            boolean hasNext() {
                return i < arrLength ? true : false;
            }

            @Override
            public
            int next() {
                if (hasNext()) {
                    int a = arr[i];
                    i++;
                    return a;
                } else {
                    return 0;
                }
            }
        };
    }
}

class MyClass{

    public static
    void main(String[] args) {
        IntSequence a = IntSequence.Of(6,9,0,80);
        while(a.hasNext()){
            System.out.print(a.next()+"  ");
        }
        IntSequence sequence1 = IntSequence.Of(10, 11);
        while (sequence1.hasNext()) {
            System.out.println(sequence1.next());
        }

    }
}