package Unit3.Ex6;

import java.math.BigInteger;
class Ex6 {

    public
    class SquareSequence implements IntSequence<BigInteger> {

        private final BigInteger STEP = BigInteger.valueOf(1);

        private BigInteger i = BigInteger.valueOf(0);

        public
        boolean hasNext() {
            return true;
        }

        @Override
        public
        BigInteger next() {
            i = i.add(STEP);
            return i.multiply(i);
        }
    }

    interface IntSequence<T> {

        boolean hasNext();

        T next();
    }

    public SquareSequence getSquareSequence (){
        return new SquareSequence();
    }


}