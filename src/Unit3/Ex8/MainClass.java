package Unit3.Ex8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class MainClass {

    static void luckySort(ArrayList<String> strings, Comparator<String> corp){
        Collections.shuffle(strings);
        System.out.println(strings.toString());
        boolean flag = true;
        for (int i = 0; i < strings.size()-1; i++) {
            if(corp.compare(strings.get(i), strings.get(i+1))>=0){
                flag = true;
            }else{
                flag = false;
                break;
            }
        }
        if (flag==false) {
            luckySort(strings,  corp);
        }else{
            System.out.println("Уря");
        }
    }

    public static void main(String[] args) {
        //создаю лист и заполняю
        ArrayList<String> myList = new ArrayList<>();
        myList.add("Sasha");
        myList.add("Taya");
        myList.add("Roma");
        myList.add("Kirill");
        System.out.println(myList.toString());
        //Collections.shuffle(myList);
        //System.out.println(myList.toString());
        //создаю объект компаратора
        Comparator<String> corp= new LengthString();
        luckySort(myList, corp);


    }
}
