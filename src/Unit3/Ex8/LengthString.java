package Unit3.Ex8;

import java.util.Comparator;

public class LengthString implements Comparator<String> {

    @Override
    public int compare(String str1, String str2) {
        return str1.length()-str2.length();
    }
}
