package Unit3.Ex2;

import javax.imageio.stream.ImageOutputStream;
import java.util.Scanner;

public class Employee implements Measurable {
    private String name;
    private double salary;
    public String str = "dfd";

    public
    void setSalary(double salary) {
        this.salary = salary;
    }

    public Employee() {
        this.name = "alex";
        this.salary = 1000;
    }

    public
    Employee(double salary) {
        this.salary = salary;
        this.name = "alex";
    }

    @Override
    public
    String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", salary=" + salary +
                '}';
    }

    @Override
    public
    double getMeasure() {
        return salary;
    }

    @Override
    public
    String getName(){
        return name;
    }

    public static double average(Measurable[] objects){
        double sum = 0;
        double arrLength = objects.length;
        for (int i = 0; i < arrLength; i++) {
            sum += objects[i].getMeasure();
        }
        double result = sum/arrLength;
        return result;
    }

    public static Measurable largest(Measurable[] objects){
        Measurable empl = objects[0];
        double arrLength = objects.length;
        for (int i = 1; i < arrLength; i++) {
            if(empl.getMeasure() < objects[i].getMeasure()){
                empl = objects[i];
            }
        }
        return empl;
    }

    public static void main(String[] args) {
        Measurable[] arr = new Employee[5];
        for (int i = 0; i < 5; i++) {
            arr[i] = new Employee(i*100);
            System.out.println(arr[i]);
        }
        double b = average(arr);
        System.out.println(b);
        Measurable largest = largest(arr);
        System.out.println(largest.getName());
        Scanner a;
        ImageOutputStream b1;
        Employee measurable = (Employee) arr[0];
    }
}


