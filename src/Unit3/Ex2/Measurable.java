package Unit3.Ex2;

public interface Measurable {

    double getMeasure();
    String getName();


}
