package Unit3.Ex13;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

public
class MyClass {

    public static File[] getSortFiles(File [] files){
        Comparator<File> comp = (first, second)-> {
            if (first.isDirectory() && second.isFile()) return -1;
            else if (first.isFile() && second.isDirectory()) return 1;
            else if (first.isFile() && second.isFile()) return 0;
            else return -1;
        };
        Arrays.sort(files,comp);
        return files;
    }


    public static File[] getListDir(String path){
        File file = new File(path);
        return file.listFiles(file1 -> file1.isDirectory());
    }


    public static void main(String [] args){
        File file = new File("C:/Users/alsa1118/Desktop");
        File [] list=file.listFiles();
        System.out.println(Arrays.toString(list));
        System.out.println(Arrays.toString(getSortFiles(list)));
    }
}


