package Unit3.Ex14;

public
class RunnableClass implements Runnable {
    static int id;
    int idElement;

    public
    RunnableClass() {
        id++;
        idElement = id;
    }

    @Override
    public
    void run() {
        System.out.println("Hello "+idElement);
    }
}
