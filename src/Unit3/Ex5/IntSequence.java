package Unit3.Ex5;

import java.util.function.Consumer;

public interface IntSequence {
    default boolean hasNext(){
        return true;
    }

    int next();

    static int[] of(int... value){
        int[] a = value;
        return value;
    }

    static void constant(int value){
        int a = value;
        while (a == value) {
            System.out.print(a);
        }
        //constant(a);
    }

    static IntSequence constant1(int value){
        return () -> value;
    }

}

class Main{

    public static
    void main(String[] args) {
        IntSequence a = IntSequence.constant1(1);
        while(a.hasNext()){
            System.out.println(a.next());
        }
    }
}
