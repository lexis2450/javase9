package Unit3.Ex5;

public
class MySequence implements IntSequence {
    int[] arr;
    int length;
    int curr;

    public
    MySequence() {
        this.curr = 0;
        arr = new int[]{0, 1, 2, 3, 4};
    }

    //@Override
    public
    boolean hasNext(){
        if(curr < arr.length) {
            return true;
        } else{
            return false;
        }
    }

    @Override
    public
    int next(){
        if(this.hasNext()) {
            return arr[curr+1];
        }else{
            return 0;
        }
    }

    public static void constants(int value){

    }

    public static
    void main(String[] args) {
        MySequence a = new MySequence();
        System.out.println();
        //System.out.println(a.next());
        IntSequence.constant(1);
        IntSequence.constant(9);
        IntSequence.constant1(9);
    }
}
