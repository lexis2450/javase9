package Unit3.Ex1;

public class Employee implements Measurable {
    public String name;
    double salary;

    public
    void setSalary(double salary) {
        this.salary = salary;
    }

    public Employee() {
        this.name = "alex";
        this.salary = 1000;
    }

    @Override
    public
    String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", salary=" + salary +
                '}';
    }

    @Override
    public
    double getMeasure() {
        return salary;
    }

    public static double average(Measurable[] objects){
        double sum = 0;
        double arrLength = objects.length;
        for (int i = 0; i < arrLength; i++) {
            sum += objects[i].getMeasure();
        }
        double result = sum/arrLength;
        return result;
    }

    public static void main(String[] args) {
        Measurable[] arr = new Employee[5];
        for (int i = 0; i < 5; i++) {
            arr[i] = new Employee();
            System.out.println(arr[i]);
        }
        double b = average(arr);
        System.out.println(b);
    }
}


