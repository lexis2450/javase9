package Unit3.Ex16;

import Unit3.Ex5.IntSequence;

import java.util.Random;


public class MyClass {
    private static Random generator = new Random();
    int low;
    int hight;

    class RandomSequence implements IntSequence{

        public RandomSequence() {
        }

        public int next() {
            return MyClass.this.low + generator.nextInt(hight-low+1);
        }
    }

    public MyClass(int low, int hight) {
        this.low = low;
        this.hight = hight;
    }

    public IntSequence randomInts(){
        return new RandomSequence();
    }

    public static
    void main(String[] args) {
        MyClass a = new MyClass(10, 100);
        IntSequence intSequence = a.randomInts();
        for (int i = 0; i < 9; i++) {
            System.out.println(intSequence.next());
        }
    }
}

