package Unit3.Ex15;


import java.util.Arrays;
import java.util.Comparator;

public class Employee implements Comparable<Employee> {
    String name;
    int salary;
    //массив работников, сортировка

    public
    Employee() {
        this.name = "name";
        this.salary = 0;
    }

    public
    Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public
    void setName(String name) {
        this.name = name;
    }

    public
    void setSalary(int salary) {
        this.salary = salary;
    }

    public
    String getName() {
        return name;
    }

    public
    int getSalary() {
        return salary;
    }

    public static Comparator<String> compareInDirection(int direction){
        return (x, y) -> direction * x.compareTo(y);
    }

    public static
    void main(String[] args) {
        Employee[] arrEmpl = new Employee[5];
        for (int i = 0; i < arrEmpl.length; i++) {
            arrEmpl[i] = new Employee();
        }
        arrEmpl[0].setName("alex");
        arrEmpl[0].setSalary(200);
        arrEmpl[1].setName("tom");
        arrEmpl[1].setSalary(9100);
        arrEmpl[2].setName("tom");
        arrEmpl[2].setSalary(1200);
        arrEmpl[3].setName("alex");
        arrEmpl[3].setSalary(200000);
        arrEmpl[4].setName("borya");
        arrEmpl[4].setSalary(10);
        Comparator<Employee> cprt = new PersonNameComparator().thenComparing(new PersonSalaryComparator());
        Arrays.sort(arrEmpl, cprt);
        for(Employee  p : arrEmpl){

            System.out.println(p.getName() + " " + p.getSalary());
        }

    }

    public
    int compareTo (Employee o2) {
        return name.compareTo(o2.name);
    }

}

class PersonNameComparator implements Comparator<Employee>{

    public int compare(Employee a, Employee b){

        return a.getName().compareTo(b.getName());
    }
}


class PersonSalaryComparator implements Comparator<Employee>{

    public int compare(Employee a, Employee b){

        if(a.getSalary()< b.getSalary())
            return 1;
        else if(a.getSalary()> b.getSalary())
            return -1;
        else
            return 0;
    }
}
