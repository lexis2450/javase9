package Unit3.Ex11;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;

public class MyClass {
    public static File[] getListDir(String path){
        File file = new File(path);
        return file.listFiles(file1 -> file1.isDirectory());
    }

    public static File[] getListDir1(String path){
        File file = new File(path);
        return file.listFiles(File::isDirectory);
    }

    public static File[] getListDir2(String path){
        File file = new File(path);
        File[] files = file.listFiles(new FileFilter() {
            @Override
            public
            boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        });
        return files;}

    public static
    void main(String[] args) {
        System.out.println(Arrays.deepToString(getListDir2("C:/Users/alsa1118/Desktop")));
    }
}



