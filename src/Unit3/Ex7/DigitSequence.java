package Unit3.Ex7;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public
class DigitSequence implements Iterator<Integer> {
    List<Integer> myList;
    int curr;

    DigitSequence(){
        this.myList = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        curr = 0;
    }

    @Override
    public
    boolean hasNext() {
        if(curr<myList.size()){
            return true;
        }else{
            return false;
        }
    }

    public void currNull(){
        curr = 0;
    }

    @Override
    public
    Integer next() {
        if(this.hasNext()==true) {
            Integer a = myList.get(curr);
            curr++;
            return a;
        }else{
            return 0;
        }
    }

    @Override
    public
    void remove() {
    }

    public static
    void main(String[] args) {
        DigitSequence mySequence = new DigitSequence();

        for (int i = 0; i < mySequence.myList.size(); i++) {
            System.out.println(mySequence.next());
        }
        mySequence.currNull();
        //mySequence.myList.set();

        mySequence.forEachRemaining((Integer a) -> a = Integer.valueOf(1000));
        mySequence.currNull();
        //mySequence.remove();
        for (int i = 0; i < mySequence.myList.size(); i++) {
            System.out.println(mySequence.next());
        }

    }
}
