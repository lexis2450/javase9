package Unit3.Ex9;

public class Greeter implements Runnable{
    int n;
    String target;

    public Greeter(int n, String target) {
        this.n = n;
        this.target = target;
    }

    @Override
    public
    void run() {
        for (int i = 0; i < this.n; i++) {
            System.out.println("Hello World " + target);
        }
    }


    public static void main(String[] args) {
        Runnable task1 = new Greeter(10, "first");
        Thread thread1 = new Thread(task1);
        Runnable task2 = new Greeter(5, "second");
        Thread thread2 = new Thread(task2);
        thread1.start();
        thread2.start();


    }
}
