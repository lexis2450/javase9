package Unit3.Ex10;

import Unit3.Ex9.Greeter;

import java.util.List;

public class MyClass implements Runnable{
    int n;
    String target;

    @Override
    public
    void run() {
        for (int i = 0; i < n; i++) {
            System.out.println("Hello World ");
        }
    }

    public static void runTogether(Runnable... task){
        Runnable[] tasks = task;
        for (int i = 0; i < tasks.length; i++) {
            Thread thread = new Thread(task[i]);
            thread.start();
        }
    }

    public static void runInOrder(Runnable... task){
        for (int i = 0; i < task.length; i++) {
            task[i].run();
        }
    }

    public static
    void main(String[] args) {
        Runnable[] tasks = new Runnable[10];
        int a = 1;
        for (int i = 0; i < 10; i++) {
            a=i;
            tasks[i] = () -> System.out.println("Hello"+Thread.currentThread().getName());
        }
        runTogether(tasks);
        runInOrder(tasks);
    }
}
