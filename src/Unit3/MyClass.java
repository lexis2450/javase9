package Unit3;

import java.io.FilenameFilter;
import java.util.Arrays;

class A implements B, C {
    public void m(){
        System.out.println("Alol");
    }

    public
    void print() {
        C.super.print();
    }
}

interface B {
    default void print(){
        System.out.println("HeloWorld");
    }
}

interface C {
    default public void print(){
        System.out.println("Привет");
    }

    static public void print1(){
        System.out.println("Привет1");
    }
}

interface D{
    void bla();
    //void metod2(int a);
    default void print(){
        System.out.println("HeloWorld");
    }
}

public
class MyClass {
    //class MyClass

    public static void met(D d){
        d.bla();
    }

    public static
    void main(String[] args) {
       met(()-> System.out.println("lol"));
       D d = ()->{
           System.out.println("lol");
       };
       String[] a = {"lol", "lala", "lalalal"};
        Arrays.sort(a, (c, b)->{
            if(c.length()>b.length()) return 1;
            else if(c.length()==b.length()) return 0;
            else return -1;
        });
    }
}
