package Unit3.Ex4;

import java.util.Arrays;

public
class MyClass implements IntSequence {
    private int i;

    @Override
    public
    boolean hasNext() {
        return true;
    }

    @Override
    public
    int next() {
        return i++;
    }



    public static
    void main(String[] args) {
        IntSequence a = new MyClass();
        int[] v = a.Of(9,0,8,7);
        System.out.println(Arrays.toString(v));
    }
}
