package Unit3.Ex4;

public
interface IntSequence {
    boolean hasNext();
    int next();

    default
    int[] Of(int... values) {
        int[] arr = values;
        return arr;
    }
}
