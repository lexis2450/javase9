package Maksim;

public class A extends B {
    String name;

    public A(String name, int age) {
        super(age);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
