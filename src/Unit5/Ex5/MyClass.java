package Unit5.Ex5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class MyClass {

    public static ArrayList<Double> readValues(String filename) throws FileNotFoundException {
        ArrayList<Double> list = new ArrayList<>();
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(filename));
            while (scanner.hasNextDouble()) {
                list.add(scanner.nextDouble());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            scanner.close();
        }
        return list;
    }

    public static void printByPrintWriter(String filename){
        ArrayList<String> list = new ArrayList<>();
        list.add("Hello");
        list.add("sasha");
        list.add("Hi");
        list.add("Maksim");
        PrintWriter pw = null;
        try{
            pw = new PrintWriter("text1");
            for(String line : list){
                pw.println(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }finally {
            pw.close();
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        ArrayList<Double> myList = readValues("textR");
        System.out.println(myList);
        printByPrintWriter("text1");
        ArrayList<String> a = new ArrayList<>(7);
        a.add("asda");
        printByPrintWriter("text1");
        //Scanner a11 = new Scanner("text1");
        //a11.nextInt();

    }
}
