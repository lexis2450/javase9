package Unit5.Ex9;

import java.util.concurrent.locks.ReentrantLock;

public class MyClass {
    //здесь будет метод
    public static Element methodHelp(){
        try(Element a = new Element()){
            ReentrantLock locker1 = new ReentrantLock();
            a.myLock.lock();
            a.myLock.unlock();
            return a;
        }
    }

    public static void main(String[] args) {
        Element element = methodHelp();
    }
}
