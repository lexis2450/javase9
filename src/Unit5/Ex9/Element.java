package Unit5.Ex9;

import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Element implements AutoCloseable {
    ReentrantLock myLock;

    public Element() {
        this.myLock = new ReentrantLock();
    }

    public Element(ReentrantLock myLock) {
        this.myLock = myLock;
    }

    @Override
    public void close(){
        this.myLock.unlock();
    }

    public void lock() {
        this.myLock.lock();
    }


}
