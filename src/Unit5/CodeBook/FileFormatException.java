package Unit5.CodeBook;

import java.io.IOException;

public class FileFormatException extends IOException {
     public FileFormatException(Throwable cause){
         initCause(cause);
     }
     public FileFormatException(String message, Throwable cause){
         super(message);
         initCause(cause);
     }
}
