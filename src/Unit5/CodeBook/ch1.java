package Unit5.CodeBook;

import java.util.Random;

public class ch1 {
    public static int randInt(int low, int hight) throws IllegalAccessException {
        if(low > hight){
            throw new IllegalAccessException(String.format("low should be <= ight but low is %d and hight%d", low, hight));
        }
        Random generator = new Random();
        return low + (int) (generator.nextDouble() * (hight - low +1));
    }

    public void writer() throws IllegalAccessException {
        int number = randInt(2, 3);
        System.out.println(number);
    }

    public static void main(String[] args) throws IllegalAccessException {
        int number = randInt(2, 3);
        System.out.println(number);
    }
}
