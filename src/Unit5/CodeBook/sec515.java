package Unit5.CodeBook;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import static java.nio.file.Path.*;

public class sec515 {
    public static void main(String[] args) throws Exception {
        ArrayList<String> lines = new ArrayList<>();
        lines.add("Stroka1");
        PrintWriter out = new PrintWriter("test515");
        for(String line : lines){
            out.println(line.toLowerCase());
        }
        out.close();

        ArrayList<String> lines2 = new ArrayList<>();
        lines2.add("Stroka2");
        try(PrintWriter out2 = new PrintWriter("test515")) {
            for(String line :lines2) {
                out2.println(line.toLowerCase());
            }
        }

        try(PrintWriter out3 = new PrintWriter("test515")){
            for (String str: lines2) {
                out3.println(str.toLowerCase());
            }
        }

        try(Scanner scanner = new Scanner("text1");
            PrintWriter out4 = new PrintWriter("test515")){
            while(scanner.hasNext()){
                out4.println(scanner.next().toLowerCase());
            }
        } catch (IOException ex){
            Throwable[] secondaryExceptions = ex.getSuppressed();
        }

        //повторное генерирование и цепочки
        try{
            ArrayList<String> lines22 = new ArrayList<>();
            lines.add("Stroka1");
            PrintWriter out22 = new PrintWriter("test515");
            for(String line : lines22){
                out.println(line.toLowerCase());
            }
            out22.close();
        }
        finally {
            /*catch (SQLException ex){
            throw new Exception("Error", ex);
        }*/
        }
    }

    public void read (String filename) throws IOException{
        try{
            ArrayList<String> lines22 = new ArrayList<>();
            lines22.add("Stroka1");
            PrintWriter out22 = new PrintWriter("test515");
            for(String line : lines22){
                out22.println(line.toLowerCase());
            }
            out22.close();
        } catch (Exception ex){
            //logger.log(level, message, ex);
            throw ex;
        }
    }
}
