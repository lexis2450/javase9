package Unit5.Ex13;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;

public class MyClass {
    int min(int[] values){
        int minN = Integer.MAX_VALUE;
        for (int i = 0; i < values.length; i++) {
            if(values[i] < minN){
                minN = values[i];
            }
        }
        assert minN == Arrays.stream(values).min().getAsInt() : "  <------  MIN_NUMBER";
        return minN;
    }

    public static void main(String[] args) {
        MyClass myClass = new MyClass();
        int length = 1000000;
        int[] arr = new int[length];
        Random generator  = new Random();
        for (int i = 0; i < length; i++) {
            arr[i] = generator.nextInt(10);
        }
        long start = System.currentTimeMillis();
        System.out.println(myClass.min(arr));
        long finish = System.currentTimeMillis();
        long timeConsumedMillis = finish - start;
        System.out.println(timeConsumedMillis + " millis");
        //System.out.println(start);
        //System.out.println(finish);
    }
}
