package Unit5.Ex11;

import java.util.logging.Logger;

public class MyClass {
    public static int factorial(int n){
        IllegalAccessException illegalAccessException = new IllegalAccessException();
        illegalAccessException.printStackTrace();
        int result;
        if (n == 1)
            return 1;
        result = factorial(n - 1) * n;
        return result;
    }

    public static void main(String[] args) throws IllegalAccessException {
        int a = factorial(13);
        System.out.println(a);
    }

}
