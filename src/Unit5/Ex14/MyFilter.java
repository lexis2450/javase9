package Unit5.Ex14;

import Unit5.speaking.statements.MyLog;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Filter;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class MyFilter {

    public static void main(String[] args) throws IOException {
        Logger log = Logger.getLogger(MyLog.class.getName());
        Filter filter = new Filter() {
            @Override
            public boolean isLoggable(LogRecord record) {
                String str = record.getMessage();
                if(str.equals("c++")) {
                    return false;
                } else {
                    return true;
                }
            }
        };
        log.setFilter(filter);
        FileHandler fileHandler = new FileHandler();
        log.addHandler(fileHandler);
        log.info("c++");
        log.info("sasha");
    }
}
