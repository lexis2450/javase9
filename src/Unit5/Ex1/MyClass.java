package Unit5.Ex1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class MyClass {

    /*public ArrayList<Double> readValues(String filename) throws FileNotFoundException {
        ArrayList<Double> list = new ArrayList<>();
        File file = new File(filename);
        try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String textR = null;
            while ((textR = reader.readLine()) != null) {
                list.add(Double.parseDouble(textR));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }*/
    public ArrayList<Double> readValues(String filename) throws FileNotFoundException {
        ArrayList<Double> list = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File(filename))) {
            while (scanner.hasNextDouble()) {
                list.add(scanner.nextDouble());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }


    public static void main(String[] args) throws IOException {
        MyClass a = new MyClass();
        ArrayList<Double> arr = a.readValues("textR");
        System.out.println(arr);

    }


}
