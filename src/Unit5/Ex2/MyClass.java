package Unit5.Ex2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class MyClass {

    public ArrayList<Double> readValues(String filename) throws FileNotFoundException, IOException {
        ArrayList<Double> list = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File(filename))) {
            while (scanner.hasNextDouble()) {
                list.add(scanner.nextDouble());
            }
        }
        return list;
    }

    public double sumOfValues(String filename)throws FileNotFoundException, IOException{
        double sum = 0;
        ArrayList<Double> list = readValues(filename);
        try (Scanner scanner = new Scanner(new File(filename))) {
            for (Double db : list){
                sum += db;
            }
        }
        return sum;
    }

    public static void main(String[] args) throws IOException {
        MyClass a = new MyClass();
        ArrayList<Double> arr = a.readValues("textR");
        System.out.println(arr);
        double db = a.sumOfValues("textR");
        System.out.println(db);
        PrintWriter writer = new PrintWriter("the-file-name.txt", "UTF-8");
        writer.println(db);
        writer.close();

    }


}
