package Unit5.Ex3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class MyClass {
    public ArrayList<Double> readValues(String filename){
        ArrayList<Double> list = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File(filename))) {
            while (scanner.hasNextDouble()) {
                list.add(scanner.nextDouble());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }

    public double sumOfValues(String filename){
        double sum = 0;
        ArrayList<Double> list = readValues(filename);
        try (Scanner scanner = new Scanner(new File(filename))) {
            for (Double db : list){
                sum += db;
            }
        } catch (FileNotFoundException e) {
            System.out.println("Дурашка");
        }
        return sum;
    }

    public static void main(String[] args) {
        try {
            Unit5.Ex2.MyClass a = new Unit5.Ex2.MyClass();
            ArrayList<Double> arr = a.readValues("./src/textR");
            System.out.println(arr);
            double db = a.sumOfValues("./src/textR");
            System.out.println(db);
        }catch (IOException e){
            //e.printStackTrace();
            System.err.println("Что то пошло не так, возможно файл не нашелся" + e.getMessage());
        }

    }
}
