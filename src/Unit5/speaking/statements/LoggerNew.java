package Unit5.speaking.statements;

import java.io.IOException;
import java.util.logging.*;

public class LoggerNew {
    public static void main(String[] args) throws IOException {
        Logger log = Logger.getLogger(MyLog.class.getName());
        log.setLevel(Level.FINE);
        log.setUseParentHandlers(false);
        Handler handler = new ConsoleHandler();
        handler.setLevel(Level.FINE);
        log.addHandler(handler);

        FileHandler handler1 = new FileHandler();
        log.addHandler(handler);
    }

}
