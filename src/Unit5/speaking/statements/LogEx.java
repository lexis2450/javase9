package Unit5.speaking.statements;

import Unit5.Ex1.MyClass;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LogEx {
    private static Logger log = Logger.getLogger(MyLog.class.getName());

    public static ArrayList<Double> readValues(String filename) throws FileNotFoundException {
        ArrayList<Double> list = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File(filename))) {
            while (scanner.hasNextDouble()) {
                list.add(scanner.nextDouble());
            }
        } catch (FileNotFoundException e) {
            log.log(Level.SEVERE, "ERROR", e);
        }
        return list;
    }


    public static void main(String[] args) throws IOException {
        ArrayList<Double> arr = readValues("text1111");
        System.out.println(arr);

    }
}
