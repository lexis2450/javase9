package Unit5.speaking.statements;

import java.util.logging.Level;
import java.util.logging.Logger;

public class MyLog {
    private static Logger log = Logger.getLogger(MyLog.class.getName());

    public void someMethod()
    {
        log.info("Some message");
    }

    public int read(String file, String pattern){
        int count = 2;
        log.entering("com.mycompany.mylib.Reaer", "read", new Object[] {file, pattern});
        log.exiting("com.mycompany.mylib.Reaer", "read", count);
        return count;
    }

    public static void main(String[] args) {
        MyLog myLog = new MyLog();
        log.info("Error");
        log.warning("ERROR");
        MyLog a = new MyLog();
        String str = "c++";
        a.read("text1", str);

        /*handlers= java.util.logging.FileHandler
        java.util.logging.FileHandler.pattern = application_log.txt
        java.util.logging.FileHandler.limit = 50
        java.util.logging.FileHandler.count = 7
        java.util.logging.FileHandler.formatter = java.util.logging.SimpleFormatter*/


    }
}
