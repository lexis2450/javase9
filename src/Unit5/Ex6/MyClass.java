package Unit5.Ex6;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class MyClass {

    public static void main(String[] args) {
        try(BufferedReader in = Files.newBufferedReader(Paths.get("textR"), StandardCharsets.UTF_8)){
            System.out.println(in.readLine());
        } catch (IOException ex){
            System.err.println("Caught IOException: " + ex.getMessage());
        }

    }

}
