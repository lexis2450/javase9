package Unit5.Ex4;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class MyClass {
    public static
    ArrayList<Double> readValues(String filename) {
        ArrayList<Double> list = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File(filename))) {
            while (scanner.hasNextDouble()) {
                list.add(scanner.nextDouble());
            }
        } catch (FileNotFoundException e) {
            //return Collections.<Double>unmodifiableList((ArrayList)Arrays.asList(100.0));
        }
        return list;
    }

    public static double sumOfValues(String filename) {
        double sum = 0;
        ArrayList<Double> list = readValues(filename);
        try (Scanner scanner = new Scanner(new File(filename))) {
            for (Double db : list){
                sum += db;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return sum;
    }

    /*public static void main(String[] args) throws IOException {
            Unit5.Ex2.MyClass a = new Unit5.Ex2.MyClass();
            ArrayList<Double> arr = a.readValues("./src/textR");
            System.out.println(arr);
            double db = a.sumOfValues("./src/textR");
            System.out.println(db);
    }*/
    public static void main(String [] args) throws IOException {
        System.out.println(sumOfValues("textR"));
    }
}
