/*
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class MainClass {
*/
/*    public static void main(String[] args) {
        //create stream
        List<String> list = Arrays.stream(args)
                .filter(s -> s.length() <= 200)
                .collect(Collectors.toList());
        System.out.println("------------------------------");

        //create parallel streams
        Arrays.asList(1, 2, 3).parallelStream().filter(a -> a > 10)
                .map(x -> x * 2)
                .collect(Collectors.toList());

        System.out.println("------------------------------");
        //create streams for int
        IntStream.range(0, 10)
                .parallel()
                .map(x -> x * 10)
                .sum();
        System.out.println("------------------------------");
        //short costructer
        Stream.of(1, 2, 3)
                .forEach(System.out::println);
        System.out.println("------------------------------");
        //give void if got null, else give once value
        String str = Math.random() > 0.5 ? "I'm feeling lucky" : null;
        Stream.ofNullable(str)
                .forEach(System.out::println);
        System.out.println("------------------------------");
        //generator elements
        Stream.generate(() -> 6)
                .limit(6)
                .forEach(System.out::println);
        System.out.println("------------------------------");
        //iterator simple
        Stream.iterate(2, x -> x + 6)
                .limit(6)
                .forEach(System.out::println);
        System.out.println("------------------------------");
        //iterator from java 9 NEW
        //you can generate discretic sequence
        Stream.iterate(2, x -> x < 25, x -> x + 6)
                .forEach(System.out::println);
        System.out.println("------------------------------");
        //combine 2 strems
        Stream.concat(
                Stream.of(10),
                Stream.of(12, 13))
                .forEach(System.out::println);
        System.out.println("------------------------------");
        //created streams from numeric interval, excluded border
        IntStream.range(0, 10)
            .forEach(System.out::println);
        System.out.println("------------------------------");
        LongStream.range(-10L, -5L)
            .forEach(System.out::println);
        System.out.println("------------------------------");
        //created streams from numeric intervals, included border
        IntStream.rangeClosed(0, 5)
                .forEach(System.out::println);
        System.out.println("------------------------------");
        LongStream.range(-8L, -5L)
                .forEach(System.out::println);
        System.out.println("------------------------------");
        ////////////////////////////////////////////
        System.out.println("------------------------------");
        //filtered your sequence
        Stream.of(120, 410, 85, 32, 314, 12)
                .filter(x -> x > 100)
                .forEach(System.out::println);
        System.out.println("------------------------------");
        //chenged using condition
        Stream.of("3", "4", "5")
                .map(Integer::parseInt)
                .map(x -> x + 10)
                .forEach(System.out::println);
        System.out.println("------------------------------");
        //like map, but more likely Sout 0, 1, 0, 1, 2, 0, 0, 1, 2
        Stream.of(2, 3, 0, 1, 3)
                .flatMapToInt(x -> IntStream.range(0, x))
                .forEach(System.out::println);
        System.out.println("------------------------------");
        //limited stream
        Stream.of(120, 410, 85, 32, 314, 12)
                .limit(4)
                .forEach(System.out::println);
        System.out.println("------------------------------");
        Stream.of(120, 410, 85, 32, 314, 12)
            .skip(2)
            .forEach(System.out::println);
        System.out.println("------------------------------");
        //sorted elements
        Stream.of(120, 410, 85, 32, 314, 12)
                .sorted()
                .forEach(System.out::println);
        System.out.println("------------------------------");
        //deletedduplicate
        IntStream.concat(
                IntStream.range(2, 5),
                IntStream.range(0, 4))
                .distinct()
                .forEach(System.out::println);
        System.out.println("------------------------------");
        //выполняет действия над элементами стрима
        //но не изменяет его
        Stream.of(0, 3, 0, 0, 5)
                .peek(x -> System.out.format("before distinct: %d%n", x))
                .distinct()
                .peek(x -> System.out.format("after distinct: %d%n", x))
                .map(x -> x * x)
                .forEach(x -> System.out.format("after map: %d%n", x));
        System.out.println("------------------------------");
        //берет элементы, если они удоволетворяют условию
        Stream.of(1, 2, 3, 4, 2, 5)
                .takeWhile(x -> x < 3)
                .forEach(System.out::println);
        System.out.println("------------------------------");
        //пропускает элементы, пока они удоволетворяют условию
        Stream.of(1, 2, 3, 4, 2, 5)
                .dropWhile(x -> x >= 3)
                .forEach(System.out::println);
        System.out.println("------------------------------");
        //////////////////////////////////////
        System.out.println("------------------------------");
        //print
        Stream.of(120, 410, 85, 32, 314, 12)
                .forEach(x -> System.out.format("%s, ", x));
        System.out.println("------------------------------");
        //count of elements
        long count = IntStream.range(0, 10)
                .flatMap(x -> IntStream.range(0, x))
                .count();
        System.out.println(count);
        System.out.println("------------------------------");
        //собирает элементы в множество или коллекцию, группирует
        List<Integer> list3 = Stream.of(1, 2, 3)
                .collect(Collectors.toList());
        System.out.println("------------------------------");
        //записывает стрим в массив
        String[] elements = Stream.of("a", "b", "c", "d")
                .toArray(String[]::new);
        System.out.println("------------------------------");

        //сумма с х0
        int sum = Stream.of(1, 2, 3, 4, 5)
                .reduce(10, (acc, x) -> acc + x);
        System.out.println("------------------------------");
        //просто сумма
        Optional<Integer> sum4 = Stream.of(1, 2, 3, 4, 5)
                .reduce((acc, x) -> acc + x);
        System.out.println(sum4.get());
        System.out.println("------------------------------");
        //поиск минимума и максимума
        int min = Stream.of(20, 11, 45, 78, 13)
                .min(Integer::compare).get();
        System.out.println("------------------------------");
        //возвращает любой эл-т или первый
        int anySeq = IntStream.range(4, 65536)
                .findAny()
                .getAsInt();
        System.out.println("------------------------------");
        int firstSeq = IntStream.range(4, 65536)
                .findFirst()
                .getAsInt();
        System.out.println("------------------------------");
        //возвращ true или false
        boolean result = Stream.of(1, 2, 3, 4, 5)
                .allMatch(x -> x <= 7);
        System.out.println("------------------------------");
        boolean result2 = Stream.of(1, 2, 3, 4, 5)
                .anyMatch(x -> x == 3);
        System.out.println("------------------------------");
        boolean result3 = Stream.of(1, 2, 3, 4, 5)
                .noneMatch(x -> x == 9);
        System.out.println("------------------------------");
        double result6 = IntStream.range(2, 16)
                .average()
                .getAsDouble();
        System.out.println("------------------------------");
        long result7 = LongStream.range(2, 16)
                .sum();
        System.out.println("------------------------------");
        LongSummaryStatistics stats = LongStream.range(2, 16)
                .summaryStatistics();
        System.out.format("  count: %d%n", stats.getCount());
        System.out.format("    sum: %d%n", stats.getSum());
        System.out.format("average: %.1f%n", stats.getAverage());
        System.out.format("    min: %d%n", stats.getMin());
        System.out.format("    max: %d%n", stats.getMax());
        System.out.println("------------------------------");
        ////
        System.out.println("------------------------------");

        //Collectors toist toSet to Collection toMap
        Deque<Integer> deque = Stream.of(1, 2, 3, 4, 5)
                .collect(Collectors.toCollection(ArrayDeque::new));
        System.out.println("------------------------------");
        //засовываем пары ключ-значение
        Map<Integer, Integer> map1 = Stream.of(1, 2, 3, 4, 5)
                .collect(Collectors.toMap(
                                Function.identity(),
                                Function.identity()
                ));
        System.out.println("------------------------------");
        Long count6 = Stream.of("1", "2", "3", "4")
                .collect(Collectors.counting());
        System.out.println(count);
        System.out.println("------------------------------");
        String s1 = Stream.of("a", "b", "c", "d")
                .collect(Collectors.joining());
        System.out.println(s1);
        // abcd
        System.out.println("------------------------------");
        String s2 = Stream.of("a", "b", "c", "d")
                .collect(Collectors.joining("-"));
        System.out.println(s2);
        // a-b-c-d
        System.out.println("------------------------------");
        String s3 = Stream.of("a", "b", "c", "d")
                .collect(Collectors.joining(" -> ", "[ ", " ]"));
        System.out.println(s3);
        // [ a -> b -> c -> d ]
        System.out.println("------------------------------");
        }*//*

}
*/
