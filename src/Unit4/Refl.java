package Unit4;

import java.lang.reflect.Field;
import java.util.Arrays;

public class Refl {

    public static void main(String[] args) throws IllegalAccessException {
        String a = "Sasha";
        System.out.println(Arrays.deepToString(a.getClass().getDeclaredMethods()));
        System.out.println(Arrays.deepToString(a.getClass().getMethods()));

        //исследование объектов
        Object obj = new Object();
        for( Field f : obj.getClass().getDeclaredFields()) {
            f.setAccessible(true);
            Object value = f.get(obj);
            System.out.println(f.getName() + ":" + value);
        }

    }


}
