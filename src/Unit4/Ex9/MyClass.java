package Unit4.Ex9;

import java.lang.reflect.Field;

public class MyClass {
    int a;
    String name;

    public MyClass() {
        a = 0;
        name = "lol";
    }

    public static StringBuilder toString1(Object obj) throws IllegalAccessException {
        StringBuilder rez = new StringBuilder();
        rez.append(obj.getClass().getName() + " [");
        for(Field fld : obj.getClass().getDeclaredFields()){
            fld.setAccessible(true);
            rez.append(fld.getName() + " : " + fld.get(obj) + " ;");
        }
        rez.append(" ]");
        return rez;
    }

    public static void main(String[] args) throws IllegalAccessException {
        MyClass a = new MyClass();
        System.out.println(toString1(a));
    }

}
