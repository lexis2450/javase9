package Unit4.Ex2;

import java.util.Objects;

public class LabeledPoint extends Point {
    private String label;

    public
    LabeledPoint(String label, double x, double y) {
        super(x, y);
        this.label = label;
    }

    public
    String getLabel() {
        return label;
    }

    @Override
    public
    String toString() {
        return getClass().getName() + " [ " + super.toString() + ", label=" + label + " ]";
    }

    @Override
    public boolean equals(Object otherObject) {
        if (!super.equals(otherObject)) return false;
        if (this == otherObject) return true;
        if (otherObject == null || getClass() != otherObject.getClass()) return false;
        LabeledPoint that = (LabeledPoint) otherObject;
        return Objects.equals(label, that.label);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), label);
    }

    public static void main(String[] args) {
        LabeledPoint a = new LabeledPoint("start", 2, 100);
        Point b = new Point(800, 300);
        //LabeledPoint c = new Point(10, 10);
        //b = a;
        LabeledPoint a1 = new LabeledPoint("start",2, 100);
       /* System.out.println(a.equals(a));
        System.out.println(a.equals(b));
        System.out.println(a.hashCode());*/
        //System.out.println(b.hashCode());
        //System.out.println(b.toString());
        System.out.println(a1.hashCode());
        System.out.println(a.hashCode());
    }


}
