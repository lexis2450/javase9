package Unit4.Ex13;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;

public class MyClass {
    public static void method(Method mtd, double min, double max, double step) throws InvocationTargetException,
            IllegalAccessException {
        int countStep = (int)((max - min)/step);
        double number = min;

        if (Modifier.isStatic(mtd.getModifiers())) { //invoke первый аргумент null
            Parameter[] arrP = mtd.getParameters();
            Object[] arrr = mtd.getParameterTypes();
            Object[] arr = mtd.getParameterTypes();
            for (int i = 0; i < countStep; i++) {
                System.out.println(mtd.invoke(null, number));
                number += step;
            }
        } else {
            Class<?> a = mtd.getClass();
            Parameter[] arrP = mtd.getParameters();
            for (int i = 0; i < countStep; i++) {
                System.out.println(mtd.invoke(a, arrP));
                number += step;
            }
        }
    }



    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException,
            IllegalAccessException {
        Class<Math> mathClass = Math.class;
        Method sqrt = mathClass.getMethod("sqrt", double.class);
      /*  System.out.println(sqrt.invoke(null, 2));
        System.out.println(sqrt.invoke(null, 4));
        System.out.println(sqrt.getModifiers());*/
        MyClass.method(sqrt, 2,16, 1);


    }

    /*public static void printMethods(Method method, double min, double max, double h) throws InvocationTargetException, IllegalAccessException {
        for(double i = min; i <= max; i += h){
            System.out.println(i + "  :  "+method.invoke(method.getDeclaringClass(),i));
        }
    }

    public static void main(String [] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method sqrtMethod = Math.class.getMethod("sqrt", double.class);
        printMethods(sqrtMethod, 200, 1000, 50);
        System.out.println("------------------ toHexString----------------------");
        Method toHasStringMetod = Double.class.getMethod("toHexString", double.class);
        printMethods(toHasStringMetod, 100, 4000, 398);
    }*/
}
