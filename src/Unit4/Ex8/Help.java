package Unit4.Ex8;

class Help {
    void method1() {
    }

    void method2() {
    }
}

class A { // нормальный класс

    static class B {
    } // статический вложенный класс

    class C {
    } // внутренний класс

    void f() {
        class D {
        } // локальный внутренний класс
    }
}