package Unit4.CodeFromBook;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class sec454 {

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor cstr = String.class.getConstructor(String.class);
        Object obj = cstr.newInstance("lala");
        Constructor<PersonReal> constructor = PersonReal.class.getConstructor(String.class);
        PersonReal personReal = constructor.newInstance("Lala");
        //personReal.setName("lol");
        System.out.println(personReal.getName());
    }
}
