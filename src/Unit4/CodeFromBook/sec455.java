package Unit4.CodeFromBook;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;

public class sec455 {

    public static void main(String[] args) throws ClassNotFoundException, IntrospectionException, InvocationTargetException, IllegalAccessException {
        Class<?> cl = Class.forName(String.class.getName());
        BeanInfo info = Introspector.getBeanInfo(cl);
        PropertyDescriptor[] props = info.getPropertyDescriptors();

        String propertyName = "class";
        Object proprtyValue = null;
        for(PropertyDescriptor prop : props) {
            if(prop.getName().equals(propertyName)) {
                proprtyValue = prop.getReadMethod().invoke(prop);
            }
        }
    }
}
