package Unit4.CodeFromBook;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class sec453 {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException,
            IllegalAccessException {
        //invoke()
        Method mtd = Math.class.getMethod("abs", int.class);
        Object result = mtd.invoke(null, -4);
        System.out.println(result.toString());

        //
        Person p = new Person() {
            @Override
            public
            int getId() {
                return 0;
            }


        };
        Method m = p.getClass().getMethod("setName", String.class);
        m.invoke(p, "anna");
        System.out.println(p.getName());
    }
}
