package Unit4.CodeFromBook;

public abstract class Person {
    private String name;

    public Person(String name) { this.name = name; }

    public
    Person() {
        this.name = "name";
    }

    public
    void setName(String name) {
        this.name = name;
    }

    public final String getName() { return name; }

    public abstract int getId();

    //public abstract  int getUID();
}