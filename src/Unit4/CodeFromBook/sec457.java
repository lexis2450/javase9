package Unit4.CodeFromBook;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

public class sec457 {

    public static void main(String[] args) {
        Object[] values = new Object[1000];
        for (int i = 0; i < values.length; i++) {
            Object value = new Integer(i);
            values[i] = Proxy.newProxyInstance(null,
                    value.getClass().getInterfaces(),
                    (Object proxy, Method m, Object[] margs) -> {
                        System.out.println(value + "." + m.getName() + Arrays.toString(margs));
                        return m.invoke(value, margs);
                    });
        }
    Arrays.binarySearch(values, new Integer(500));
    }
}
