package Unit4.CodeFromBook;

import Unit3.Ex2.Employee;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Arrays;

public class sec452 {

    public static void main(String[] args) throws IllegalAccessException, NoSuchFieldException {
        //String a = "Sasha";
        //System.out.println(Arrays.deepToString(a.getClass().getDeclaredMethods()));
        //System.out.println(Arrays.deepToString(a.getClass().getMethods()));

        //исследование объектов
        Employee obj = new Employee();
        for( Field f : obj.getClass().getFields()) {
            f.setAccessible(true);
            Object value = f.get(obj);
            System.out.println(f.getName() + ":" + value);
        }

        //установитьт значение  в поле
        Field f = obj.getClass().getField("salary") ;
        f.setAccessible(true);
        double value = f.getDouble(obj);
        f.setDouble(obj, value * 1.1);
    }

}
