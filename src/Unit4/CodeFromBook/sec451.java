package Unit4.CodeFromBook;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class sec451 {

    public static void main(String[] args) throws ClassNotFoundException {

        Class<?> cl = Class.forName(String.class.getName());
        //while(cl != null) {
            for(Method m : cl.getMethods()) {
                System.out.println(
                        Modifier.toString(m.getModifiers())+ " " +
                                m.getReturnType().getCanonicalName() + " " +
                                m.getName() + Arrays.toString(m.getParameters()));
            }
            //cl = cl.getSuperclass();
        //}
    }
}
