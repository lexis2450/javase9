package Unit4.CodeFromBook;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class sec456 {

    public static Object[] badCopyOf(Object[] array, int newLength){
        Object[] newArray = new Object[newLength];
        for(int i = 0; i < Math.min(array.length, newLength); i++){
            newArray[i] = array[i];
        }
        return newArray;
    }

    public static Object goodCopy(Object array, int newLength){
        Class<?> cl = array.getClass();
        if(!cl.isArray())return null;
        Class<?> componentType = cl.getComponentType();
        int length = Array.getLength(array);
        Object newArray = Array.newInstance(componentType, newLength);
        for (int i = 0; i < Math.min(length, newLength); i++) {
            Array.set(newArray, i, Array.get(array, i));
        }
        return newArray;
    }

    public static void main(String[] args) {
        Person[] friends = new Person[3];
        friends[0] = new PersonReal();
        friends[0].setName("Lena");
        friends[1] = new PersonReal();
        friends[2] = new PersonReal();
        friends[1].setName("Tom");
        friends[2].setName("Pety");

        friends = Arrays.copyOf(friends, friends.length + 1);

        int[] primes = {2, 3, 4, 5, 7, 11};
        primes = (int[]) goodCopy(primes, 10);
        Person[] objects = (Person[])badCopyOf(friends, 3);
        System.out.println(Arrays.deepToString(objects));
    }

}
