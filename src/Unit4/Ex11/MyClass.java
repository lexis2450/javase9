package Unit4.Ex11;

import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MyClass {
    public static void main(String[] args) throws ClassNotFoundException, InvocationTargetException,
            IllegalAccessException, NoSuchMethodException {
        PrintStream out = System.out;
        Class<? extends PrintStream> aClass = out.getClass();
        Method println = aClass.getMethod("println", String.class);
        long start = System.currentTimeMillis();
        println.invoke(out, "hello world");
        long end = System.currentTimeMillis();
        System.out.println(end-start);

        long start1 = System.currentTimeMillis();
        out.println("Hello World");
        long end1 = System.currentTimeMillis();
        System.out.println(end1-start1);
        /*for(Method mtd : Class.forName("java.lang.System").getMethods()){
            if(mtd.getName().equals("setOut")){
                for(Method mtd1 : Class.forName("java.lang.System").getMethods()){
                    if(mtd.getName() == "println"){
                        mtd1.invoke("Hello World");
                    }
                }
            }
        }*/


    }
}
