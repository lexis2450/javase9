package Unit4.Ex4;

public class Line extends Shape{
    private Point from;
    private Point to;

    public Line(Point from, Point to) {
        super(from);
        this.to = to;
    }

    @Override
    public Point getCenter() {
        Point centerPoint = new Point(0, 0);
        centerPoint.setX(to.x - from.x);
        centerPoint.setY(to.y - from.y);
        return centerPoint;
    }

    @Override
    public
    String toString() {
        return "Line{" +
                "from=" + from +
                ", to=" + to +
                '}';
    }
}
