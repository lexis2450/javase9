package Unit4.Ex4;

import java.util.Objects;

public
class Point {
    protected double x;
    protected double y;

    public
    Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public
    double getX() {
        return x;
    }

    public
    double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return getClass().getName() + " [ " + "x=" + x + ", y=" + y + " ]";
    }

    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        Point point = (Point) otherObject;
        return Double.compare(point.x, x) == 0 && Double.compare(point.y, y) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
