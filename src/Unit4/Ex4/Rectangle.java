package Unit4.Ex4;

public class Rectangle extends Shape{
    private double width;
    private double height;

    public Rectangle (Point topLeft, double width, double height) {
        super(topLeft);
        this.width = width;
        this.height = height;
    }

    @Override
    public Point getCenter() {
        Point centerPoint = new Point(0, 0);
        centerPoint.setX((height/2));
        centerPoint.setY((height/2));
        return centerPoint;
    }
}
