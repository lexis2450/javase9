package Unit4.Ex4;

import Unit4.Ex4.Point;

public abstract class Shape {
    private Point myPoint;

    public Shape(Point myPoint) {
        this.myPoint = myPoint;
    }

    public
    Point getMyPoint() {
        return myPoint;
    }

    public void moveBy(double dx, double dy){
        myPoint.setX(myPoint.getX()+dx);
        myPoint.setY(myPoint.getY()+dy);
    }

    public abstract Point getCenter();

    public static void main(String[] args) {
        Point a = new Point(2, 2);
        Circle v = new Circle(a,10);
        System.out.println(v.getCenter());
        v.moveBy(3, 3);
        System.out.println(v.getCenter());
    }

}
