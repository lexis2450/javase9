package Unit4.Ex6;

import java.util.Objects;

public class DiscountedItem extends Item {
    private double discount;
    double a;

    public DiscountedItem(String description, double price, double discount) {
        super(description, price);
        this.discount = discount;
    }

    public boolean equals(Object otherObject) {
        if (!super.equals(otherObject)) return false;
        DiscountedItem other = (DiscountedItem) otherObject;
        return discount == other.discount;
    }

    public int hashCode() {
        return Objects.hash(super.hashCode(), discount);
    }

    @Override
    public void printA() {
        super.printA();
    }
}
