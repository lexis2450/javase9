package Unit4.Ex5;

public class Rectangle extends Shape implements Cloneable {
    private double width;
    private double height;

    public Rectangle() {
        super();
    }

    public Rectangle (Point topLeft, double width, double height) {
        super(topLeft);
        this.width = width;
        this.height = height;
    }

    @Override
    public
    Point getCenter() {
        Point centerPoint = new Point(0, 0);
        centerPoint.setX((getMyPoint().getX() + width/2));
        centerPoint.setY((getMyPoint().getY() - height/2));
        return centerPoint;
    }

    public Rectangle clone() throws CloneNotSupportedException {
        /*Rectangle cloned = new Rectangle();
        Point a = new Point();
        a.x = super.getMyPoint().getX();
        a.y = super.getMyPoint().getY();
        cloned.width = this.width;
        cloned.height = this.height;
        return cloned;*/
        Rectangle clone = (Rectangle) super.clone();
        clone.setMyPoint(getMyPoint().clone());
        return clone;
    }
}
