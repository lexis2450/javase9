package Unit4.Ex5;

public class Line extends Shape {
    private Point to;

    public Line(Point from, Point to) {
        super(from);
        to = new Point(to.getX(), to.getY());
        this.to = to;
    }

    public Point getTo() {
        return to;
    }

    @Override
    public Point getCenter() {
        Point centerPoint = new Point(0, 0);
        centerPoint.setX(to.x - super.getMyPoint().x);
        centerPoint.setY(to.y - super.getMyPoint().y);
        return centerPoint;
    }

    public Line clone(){
        Point a = new Point();
        a.x = super.getMyPoint().getX();
        a.y = super.getMyPoint().getY();
        Point b = new Point();
        b.x = getTo().getX();
        b.y = getTo().getY();
        Line cloned = new Line(a, b);
        return cloned;
    }
}
