package Unit4.Ex5;

public abstract class Shape {
    private Point myPoint;

    public Shape(Point myPoint) {
        this.myPoint = new Point (myPoint.getX(), myPoint.getY());
    }

    public Shape() {
        myPoint = new Point(0, 0);
    }


    public Point getMyPoint() {
        return myPoint;
    }

    public void moveBy(double dx, double dy){
        myPoint.setX(myPoint.getX()+dx);
        myPoint.setY(myPoint.getY()+dy);
    }

    public void setMyPoint(Point myPoint) {
        this.myPoint = myPoint;
    }

    public abstract Point getCenter();

    public static void main(String[] args) throws CloneNotSupportedException {
        Point a = new Point(0, 2);
        Point c = new Point(3, 3);
        Rectangle v = new Rectangle(a,10, 2);
        Rectangle clone = v.clone();
        clone.moveBy(3, 3);
        System.out.println(v.getCenter());
        System.out.println(clone.getCenter());
     /*   System.out.println(v.getCenter());
        Rectangle rec = v.clone();
        System.out.println(v.toString());
        System.out.println(rec.toString());

        Line t = new Line(a, c);
        System.out.println(t.toString());
        Line tNew  = t.clone();
        System.out.println(tNew.toString());

        Circle ssh = new Circle(a, 10);
        Circle ssHnew = ssh.clone();*/

    }


}
