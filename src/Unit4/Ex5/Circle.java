package Unit4.Ex5;

public class Circle extends Shape {
    private double radius;

    public Circle(Point center, double radius) {
        super(center);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public Circle clone(){
        Point a = new Point();
        a.x = super.getMyPoint().getX();
        a.y = super.getMyPoint().getY();
        Circle cir = new Circle(a, this.getRadius());
        return cir;
    }

    @Override
    public Point getCenter() {
        return getMyPoint();
    }
}
