package Unit4.Ex5;

import java.io.Serializable;
import java.util.Objects;

public
class Point implements Serializable, Cloneable {
    protected double x;
    protected double y;

    public
    Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point() {
        this.x = 0;
        this.y = 0;
    }

    public
    double getX() {
        return x;
    }

    public
    double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return getClass().getName() + " [ " + "x=" + x + ", y=" + y + " ]";
    }

    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        Point point = (Point) otherObject;
        return Double.compare(point.x, x) == 0 && Double.compare(point.y, y) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public Point clone() throws CloneNotSupportedException {
        /*Point cloned = new Point();
        cloned.x = this.x;
        cloned.x = this.x;
        return cloned;*/
        return (Point) super.clone();
    }


}
