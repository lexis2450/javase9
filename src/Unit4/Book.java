package Unit4;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;

public class Book {
    //анонимные подклассы
    ArrayList<String> names = new ArrayList<String>(100){
        public void add(int index, String element){
            super.add(index, element);
            System.out.println("Adding %s at %d\n" + element + index);
        }
    };

    public
    Book() throws ClassNotFoundException {
    }

    public static void main(String[] args) {
        ArrayList<String> friends = new ArrayList<>();
        friends.add("Harry");
        friends.add("Sally");
        invite(friends);
        //если используем 1 раз можно как анонимный подкласс
        invite(new ArrayList<String>() {{add("Harry"); add("Sally");}});

        //рефлексия
        String a = new String();
        System.out.println(a.getClass().getDeclaredMethods());
        System.out.println(a.getClass().getMethods());
    }

    private static
    void invite(ArrayList<String> friends) {
    }

    Class<?> cl = Class.forName(String.class.getName());
    /*while(cl != null) {
        for(Method m : cl.getDeclaredMethods()) {
            System.out.println(
                    Modifier.toString(m.getModifiers())+ " " +
                            m.getReturnType().getCanonicalName() + " " +
                            m.getName() + Arrays.toString(m.getParameters()));
        }
        cl = cl.getSuperclass();
    }*/


}
