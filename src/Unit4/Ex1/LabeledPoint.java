package Unit4.Ex1;

public class LabeledPoint extends Point {
    String label;

    public
    LabeledPoint(String label, double x, double y) {
        super(x, y);
        this.label = label;
    }

    public
    String getLabel() {
        return label;
    }

    public static void main(String[] args) {
        LabeledPoint a = new LabeledPoint("start", 2, 100);
        Point b = new Point(800, 300);
        //LabeledPoint c = new Point(10, 10);
        b = a;
        System.out.println(a.getLabel());
    }


}
