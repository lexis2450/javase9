package unit10.Eckel;

import java.util.concurrent.ThreadFactory;

public class ThreadFactory1 implements ThreadFactory {
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
    }
}
