package unit10.ex6;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MyClass implements Runnable{

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println("I'm ----> " + i);
        }
    }

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(new MyClass());
        int a = (int) (Math.random()*10);
        try {
            TimeUnit.MILLISECONDS.sleep(a);
            System.out.println("timeout " + a);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("-------------------------------");
        ExecutorService executorService1 = Executors.newFixedThreadPool(3);
        ExecutorService executorService2 = Executors.newSingleThreadExecutor();
        for (int i = 0; i < 3; i++) {
            executorService2.execute(new MyClass());
        }
    }
}
