package unit10.ex5;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Fibonacci implements Callable<String> {
    private static int n;

    Fibonacci(int n){
        this.n = n;
    }

    public static List<Integer> fib(){
        List<Integer> fbn = new ArrayList<>(n);
        fbn.add(1);
        fbn.add(1);
        int n0 = 1;
        int n1 = 1;
        int n2;
        System.out.print(n0+" "+n1+" ");
        for(int i = 3; i <= n; i++){
            n2=n0+n1;
            fbn.add(n2);
            n0=n1;
            n1=n2;
        }
        return fbn;
    }

    public static String sum(){
        List<Integer> list = fib();
        return (list.stream().reduce(0, (a, b) -> a + b)).toString();
    }

    @Override
    public String call() throws Exception {
        return sum();
    }

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        Future<String> submit = executorService.submit(new Fibonacci(10));
        try {
            String s = submit.get();
            System.out.println(s);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }
}
