package unit10.ex7;

//: concurrency/SimpleDaemons.java
// Потоки-демоны не препятствуют завершению работы программы.
import java.io.IOException;
import java.util.concurrent.*;




class SimpleDaemons implements Runnable {
    public void run() {
        try {
            while (true) {
                try {
                    TimeUnit.MILLISECONDS.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread() + " " + this);
            }
        } finally {

        }
    }

    public static void main(String[] args) throws Exception {
        for (int i = 0; i < 10; i++) {
            /*Thread daemon = new Thread(new SimpleDaemons());
            daemon.setDaemon(true); // Необходимо вызвать перед start() daemon.start();
            System.out.println("All daemons started");
            TimeUnit.MILLISECONDS.sleep(175);*/
            Thread d = new Thread(new Daemon());
            d.setDaemon(true);
            d.start();
            System.out.println("d.isDaemon() = " + d.isDaemon() + ", ");
// Дать возможность потокам-демонам // завершить их стартовые процессы:
            TimeUnit.SECONDS.sleep(15);

        }
    }
}