package unit10.ex4;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MyClass {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < 5; i++) {
            executorService.execute(new unit10.ex1.MyClass());
        }
        executorService.shutdown();
        System.out.println("--------------------");

        ExecutorService executorService1 = Executors.newCachedThreadPool();
        for (int i = 0; i < 5; i++) {
            executorService1.execute(new unit10.ex1.MyClass());
        }
        executorService1.shutdown();
        System.out.println("--------------------");

        ExecutorService executorService2 = Executors.newCachedThreadPool();
        for (int i = 0; i < 5; i++) {
            executorService2.execute(new unit10.ex1.MyClass());
        }
        executorService2.shutdown();
        System.out.println("--------------------");
    }
}
