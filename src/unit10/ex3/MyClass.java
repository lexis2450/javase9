package unit10.ex3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MyClass implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println("Write № ---> " + i);
            Thread.yield();
        }
    }

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        System.out.println("START");
        for (int i = 0; i < 5; i++) {
            executorService.execute(new MyClass());
        }
        executorService.shutdown();
        System.out.println("STOP");
        System.out.println("--------------------");
        ExecutorService executorService1 = Executors.newFixedThreadPool(3);
        System.out.println("START1");
        for (int i = 0; i < 5; i++) {
            executorService1.execute(new MyClass());
        }
        executorService1.shutdown();
        System.out.println("STOP1");
        System.out.println("---------------------");
        System.out.println("START2");
        ExecutorService executorService2 = Executors.newSingleThreadExecutor();
        for (int i = 0; i < 5; i++) {
            executorService2.execute(new MyClass());
        }
        executorService2.shutdown();
        System.out.println("STOP2");


    }
}
