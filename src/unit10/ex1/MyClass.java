package unit10.ex1;

import java.sql.SQLOutput;

public class MyClass implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println("Write № ---> " + i);
            Thread.yield();
        }
    }

    public static void main(String[] args) {
        System.out.println("START");
        MyClass myClass = new MyClass();
        myClass.run();
        System.out.println("STOP");


        System.out.println("START111");
        for (int i = 0; i < 4; i++) {
            new Thread(new MyClass()).start();
        }
        Thread thread = new Thread(myClass);
        System.out.println("STOP111");
    }
}
