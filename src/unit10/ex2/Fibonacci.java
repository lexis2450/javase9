package unit10.ex2;

public class Fibonacci implements Runnable {
    private static int n;

    public static void fib(){
        int n0 = 1;
        int n1 = 1;
        int n2;
        System.out.print(n0+" "+n1+" ");
        for(int i = 3; i <= n; i++){
            n2=n0+n1;
            System.out.print(n2+" ");
            n0=n1;
            n1=n2;
        }
        System.out.println();
    }

    public Fibonacci(int n) {
        this.n = n;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    @Override
    public void run() {
        fib();
    }

    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            new Thread(new Fibonacci(10)).start();
        }
    }
}
