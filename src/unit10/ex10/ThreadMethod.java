package unit10.ex10;


import unit10.ex5.Fibonacci;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static unit10.ex5.Fibonacci.sum;

public class ThreadMethod {
    private int countDown = 5;
    private Thread t;
    private String name;
    private int n;
    String ftr = "";

    public ThreadMethod(int n, String name) {
        this.name = name;
        this.n = n;
    }

    public String runTask() {
        //Future<String> ftr;
        if (t == null) {
            t = new Thread(name) {
                public String run( int countDown) {
                    ftr = sum();
                    System.out.println(ftr);
                    return ftr.toString();
                }

                public String sum(){
                    List<Integer> list = fib(countDown);
                    return (list.stream().reduce(0, (a, b) -> a + b)).toString();
                }

                public List<Integer> fib(int n) {
                    List<Integer> fbn = new ArrayList<>(n);
                    fbn.add(1);
                    fbn.add(1);
                    int n0 = 1;
                    int n1 = 1;
                    int n2;
                    System.out.print(n0 + " " + n1 + " ");
                    for (int i = 3; i <= n; i++) {
                        n2 = n0 + n1;
                        fbn.add(n2);
                        n0 = n1;
                        n1 = n2;
                    }
                    return fbn;
                }
                
                public String toString() {
                    return getName() + ": " + countDown + "  " + ftr + "  " + ftr;

                }
            };
            t.start();
        }
        return ftr;
    }
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        new ThreadMethod(10, "str").runTask();


    }

}
