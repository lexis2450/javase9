package unit10.ex9;

import java.util.concurrent.ThreadFactory;

public class ThreadFactory1 implements ThreadFactory {
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
        t.setPriority(Thread.NORM_PRIORITY);
        t.setDaemon(true);
        return t;
    }
}
