package unit10.ex9;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//: concurrency/SimplePriorities.java // Shows the use of thread priorities, import java.util.concurrent.*;
public class SimplePriorities implements Runnable {
    private int countDown = 5;
    private volatile double d; // Без оптимизации
    private int priority;

    public SimplePriorities(int priority) {
        this.priority = priority;
    }

    public SimplePriorities() {

    }

    public String toString() {
        return Thread.currentThread() + ": " + countDown;
    }

    public void run() {
        //Thread.currentThread().setPriority(priority);
        while (true) {
// Высокозатратная прерываемая операция:
            for (int i = 1; i < 100000; i++) {
                d += (Math.PI + Math.E) / (double) i;
                if (i % 1000 == 0)
                    Thread.yield();

                System.out.println(this);
                if (--countDown == 0)return;
            }
        }
    }
    public static void main (String[] args) {
        ExecutorService exec = Executors.newCachedThreadPool(new ThreadFactory1());
        for (int i = 0; i < 5; i++){
            exec.execute(
                    new SimplePriorities());
        System.out.println(Thread.currentThread().toString());
    }
    }
}