package unit7.ex3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class MyClass {
    public static <E> Set<E> union(Set<E> a, Set<E> b){
        HashSet<E> es = new HashSet<>();
        es.addAll(a);
        es.addAll(b);
        return es;
    }

    public static <E> Set<E> intersection(Set<E> a, Set<E> b){
        HashSet<E> es = new HashSet<>();
        es.addAll(a);
        es.retainAll(b);
        return es;
    }

    public static <E> Set<E> difference(Set<E> a, Set<E> b){
        HashSet<E> es = new HashSet<>();
        es.addAll(a);
        es.removeAll(b);
        return es;
    }

    public static void main(String[] args) {
        Set<Integer> a = new HashSet<>();
        a.add(1);
        a.add(2);
        a.add(3);
        Set<Integer> b = new HashSet<>();
        b.add(3);
        b.add(4);
        b.add(5);
        Set<Integer> res1 = union(a, b);
        Set<Integer> res2 = intersection(a, b);
        Set<Integer> res3 = difference(a, b);
        System.out.println();
    }


}
