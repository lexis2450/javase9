package unit7.ex11;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class MyClass {
    public static void main(String[] args) throws IOException {
        String pathName = "C:\\Users\\alsa1118\\IdeaProjects\\javase9\\text1";
        String everything;
        try(BufferedReader br = new BufferedReader(new FileReader(pathName))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while ( line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            everything = sb.toString();
        }
        List<String> myList = new ArrayList<>();
        String[] words = everything.split(" ");
        words[words.length - 1] = words[words.length - 1].split("\r\n")[0];
        for (String word : words) {
            myList.add(word);
        }
        System.out.println(myList);
        List<String> view = myList.subList(1, myList.size() - 2);
        Random a = new Random();
        Collections.shuffle(view, a);
        System.out.println(myList);
    }
}
