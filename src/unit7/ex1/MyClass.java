package unit7.ex1;

import java.util.BitSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static java.util.Collections.min;

public class MyClass {
    public static void eratosfen1(int n){
        Set<Integer> mySet = new HashSet<>();
        Set<Integer> myRes = new HashSet<>();
        Integer number = 2;
        while(number <= n){
            mySet.add(number);
            number++;
        }
        int minValue = min(mySet);
        int i = 0;
        boolean flag = true;
        while(Math.pow(minValue, 2) <= n){
            while(flag) {
                mySet.remove(minValue * (minValue + i));
                if(minValue * (minValue + i) > n)
                    flag = false;
                    i++;
            }
            flag = true;
            i = 0;
            myRes.add(minValue);
            mySet.remove(minValue);
            minValue = min(mySet);
        }
        myRes.addAll(mySet);
        System.out.println(myRes);
    }

    public static void eratosfen2(int n){
        BitSet mySet = new BitSet(n + 1);
        int s = 2;
        while(s <= n){
            mySet.set(s);
            s++;
        }
        s = 2;
        int j = 0;
        //boolean flag = true;
        while(s*s < n){
            while(mySet.get(s) && (s * (s + j)) <= n){
                mySet.clear(s * (s + j));
                j++;
            }
            j = 0;
            s = mySet.nextSetBit(s + 1);
        }
        System.out.println(mySet);
    }

    public static void main(String[] args) {
        eratosfen1(30);
        eratosfen2(30);
        HashSet<Integer> hashSet = new HashSet<>();
        int n = 30;
        for(int i = 2; i <= n;i++) {
            hashSet.add(i);
        }
        Iterator<Integer> iterator = hashSet.iterator();
        Iterator<Integer> iterator1 = hashSet.iterator();

        while(iterator.hasNext()){
            int s = 1;
            int p = s;
            while((p * s) <= n){
                hashSet.remove(s * p);
                p++;
            }
        }
        hashSet.forEach(x -> System.out.println(x));
        System.out.println();
    }
}
