package unit7.ex2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

public class MyClass {
    public static ArrayList<String> toUpper1(ArrayList<String> arr){
        int i = 0;
        Iterator itr = arr.iterator();
        while(itr.hasNext()){
            String curr = ((String) itr.next()).toUpperCase();
            arr.set(i, curr);
            //((ListIterator) itr).set(curr);
            i++;
        }
        return arr;
    }

    public static ArrayList<String> toUpper2(ArrayList<String> strings) {
        for (int i = 0; i < strings.size(); i++) {
            String curr = (strings.get(i)).toUpperCase();
            strings.set(i, curr);
        }
        return strings;
    }

    public static ArrayList<String> toUpper3(ArrayList<String> strings) {
        strings.replaceAll(String::toUpperCase);
        return strings;
    }

    public static void main(String[] args) {
        ArrayList<String> strings = new ArrayList<>();
        strings.add("lol");
        strings.add("HI");
        strings.add("alex");
        ArrayList<String> a = toUpper1(strings);
        ArrayList<String> b = toUpper2(strings);
        ArrayList<String> c = toUpper3(strings);
        System.out.println();
    }
}
