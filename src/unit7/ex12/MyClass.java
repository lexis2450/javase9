package unit7.ex12;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public
class MyClass {
    public static void main(String[] args) throws IOException {
        String pathName = "C:\\Users\\alsa1118\\IdeaProjects\\javase9\\text1";
        try(BufferedReader br = new BufferedReader(new FileReader(pathName))) {
            String lines;
            while((lines = br.readLine())!=null){
                String[] a = lines.split("\\PL+");
                printShuffle(a);
            }
        }
    }

    public static void printShuffle(String[] words){
        words[0] = words[0].toLowerCase();
        List<String> list = Arrays.asList(words);
        Collections.shuffle(list);
        System.out.println(list);
    }

}
