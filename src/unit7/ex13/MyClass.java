package unit7.ex13;

import java.util.LinkedHashMap;
import java.util.Map;

public class MyClass {
    static class Cache<K, V> extends LinkedHashMap<K, V> {
        private final int capacity;

        public Cache(int initialCapacity) {
            super(initialCapacity);
            this.capacity = initialCapacity;
        }

        @Override
        protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
            return this.size() > this.capacity;
        }
    }

    public static void main(String[] args) {
        Cache a = new Cache(5);
        a.put(1, 12);
        a.put(2, 33);
        a.put(3, 98);
        a.put(4, 182);
        a.put(5, 109);
        a.put(6, 1);
    }
}
