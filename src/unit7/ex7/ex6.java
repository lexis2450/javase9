package unit7.ex7;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class ex6 {
    public static void main(String[] args) throws IOException {
        String pathName = "C:\\Users\\alsa1118\\IdeaProjects\\javase9\\text1";
        String everything;
        try(BufferedReader br = new BufferedReader(new FileReader(pathName))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while ( line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            everything = sb.toString();
        }
        Map<String, Integer> myMap = new TreeMap<String, Integer>();
        String[] words = everything.split(" ");
        words[words.length - 1] = words[words.length - 1].split("\r\n")[0];
        for (String word : words) {
            myMap.merge(word, 1, (oldValue, value) -> oldValue + value);
        }
        System.out.println(myMap.toString());
    }

}
