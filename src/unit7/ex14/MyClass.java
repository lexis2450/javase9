package unit7.ex14;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class MyClass {
    static private List<Integer> list = new ArrayList<>();

    public List<Integer> getView(){
        return Collections.unmodifiableList(list);
    }

    @Override
    public
    String toString() {
        return "MyClass{" +
                "list=" + list +
                '}';
    }

    public static void main(String[] args) {
        MyClass a = new MyClass();
        for(Integer i : MyClass.list){
            MyClass.list.add(i);
        }
        System.out.println(a.toString());
        List<Integer> view = a.getView();

    }
}
