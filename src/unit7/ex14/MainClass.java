package unit7.ex14;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainClass  {

    public static
    void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }
        System.out.println(list);
        List<Integer> view = Collections.unmodifiableList(list);
        System.out.println(view);
        //view.add(12);
        System.out.println(view);
    }
}
