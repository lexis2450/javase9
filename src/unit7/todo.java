package unit7;

import Unit6.Predicate;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

public class todo {
    public static void main(String[] args) {
        Predicate<Integer> pred = arg -> arg.equals(2);

        Function<Integer, Integer> func = t -> t++;

        Supplier<String> suppl = () -> ":)" + ":(";

        Consumer<Double> consumer = t -> t.toString();

        UnaryOperator<Integer> unOpr = t -> t;


    }
}
