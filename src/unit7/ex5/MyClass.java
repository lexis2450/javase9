package unit7.ex5;

import java.util.*;

public class MyClass {
    private static <E> void randomAccess(List<E> list, int i, int j) {
        Collections.swap(list, i, j);
        list.set(i, list.set(j, list.get(i)));
    }

    //последовательный доступ
    private static <E> void sequentialAccess(List<E> list, int i, int j) {
        ListIterator<E> iter = list.listIterator(i);
        E first = (E) iter.next();
        for (int x = i + 1; x < j; x++) {
            iter.next();
        }
        E second = (E) iter.next();
        iter.set(first);
        for (int x = j; x >= i; x--) {
            iter.previous();
        }
        iter.set(second);

    }

    public static void swap(List<?> list, int i, int j) {
        if (i >= list.size() || j >= list.size()) {
            throw new IllegalArgumentException("error");
        }
        if (list instanceof RandomAccess) {
            randomAccess(list, i, j);
        } else {
            sequentialAccess(list, i, j);
        }
    }

    public static void main(String[] args) {
        List<Integer> a = new ArrayList<>();
        a.add(0);
        a.add(1);
        a.add(2);
        a.add(3);
        a.add(4);
        a.add(5);
        a.add(6);
        swap(a, 0, 1);

        Collection<Integer> a1 = new HashSet<>();
        a1.add(0);
        a1.add(1);
        a1.add(2);
        a1.add(3);
        a1.add(4);
        a1.add(5);
        a1.add(6);
        //swap(a1, 0, 1);
        //RandomAccess
    }
}
