package unit7.Code;

import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Set;

public
class sec753 {
    enum Weekday { MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY };
    Set<Weekday> always = EnumSet.allOf(Weekday.class);
    Set<Weekday> never = EnumSet.noneOf(Weekday.class);
    Set<Weekday> workday = EnumSet.range(Weekday.MONDAY, Weekday.FRIDAY);
    Set<Weekday> mwf = EnumSet.of(Weekday.MONDAY, Weekday.WEDNESDAY, Weekday.FRIDAY);


    public static
    void main(String[] args) {
        Set<sec753.Weekday> always = EnumSet.allOf(sec753.Weekday.class);
        Set<sec753.Weekday> never = EnumSet.noneOf(sec753.Weekday.class);
        Set<sec753.Weekday> workday = EnumSet.range(sec753.Weekday.MONDAY, sec753.Weekday.FRIDAY);
        Set<sec753.Weekday> mwf = EnumSet.of(sec753.Weekday.MONDAY, sec753.Weekday.WEDNESDAY, sec753.Weekday.FRIDAY);
        EnumMap<Weekday, String> personlnCharge = new EnumMap<>(Weekday.class);
        personlnCharge.put(Weekday.MONDAY, "Fred");
    }

}
