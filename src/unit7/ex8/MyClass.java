package unit7.ex8;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class MyClass {
    public static void main(String[] args) throws IOException {
        String pathName = "C:\\Users\\alsa1118\\IdeaProjects\\javase9\\text1";
        String everything;
        try(BufferedReader br = new BufferedReader(new FileReader(pathName))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while ( line != null) {
                sb.append(line);
                line = br.readLine();
            }
            everything = sb.toString();
        }
        Set mySet = new HashSet();
        String[] words = everything.split(" ");
        //words[words.length - 1] = words[words.length - 1].split("\r\n")[0];
        for (String word : words) {
            mySet.add(word);
        }
        System.out.println(mySet.toString());

    }
}
