package todo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public
class MyClass {
    public static
    Comparator<String> comparelnDirection(int direction) {
        return (x, y) -> direction * x.compareTo(y);
    }

    public static void main(String[] args) {
        String[] friends = new String[3];
        friends[0] = "mol";
        friends[1] = "lol";
        friends[2] = "pol";
        Arrays.sort(friends, comparelnDirection(-1));

    }


}
