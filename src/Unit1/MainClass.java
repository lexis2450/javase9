package Unit1;

import java.math.BigInteger;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class MainClass {

    public static void ex1(){
        System.out.print("Enter: ");
        int number = 0;
        Scanner in = new Scanner(System.in);
        if (in.hasNextInt()) {
            number = in.nextInt();
        }else{
            System.out.print("Enter: ");
        }
        String str1 = Integer.toString(number, 2);
        String str2 = Integer.toString(number, 8);
        String str3 = Integer.toString(number, 16);
        System.out.println(str1);
        System.out.println(str2);
        System.out.println(str3);
    }

    public static void ex3_1(){
        System.out.print("3 numbers: ");
        Scanner in = new Scanner(System.in);
        int numb1 = in.nextInt();
        int numb2 = in.nextInt();
        int numb3 = in.nextInt();
        int numbMax = numb1>numb2 ? (numb1>numb3 ? numb1 : numb3) : (numb2>numb3 ? numb2 : numb3);
        System.out.println(numbMax);
    }

    public static void ex3_2(){
        System.out.print("3 numbers: ");
        Scanner in = new Scanner(System.in);
        int numb1 = in.nextInt();
        int numb2 = in.nextInt();
        int numb3 = in.nextInt();
        int numbMax = Integer.MIN_VALUE;
        numbMax = Math.max(numb1, numb2);
        numbMax = Math.max(numbMax, numb3);
        System.out.println(numbMax);
    }

    public static void ex4(){
        double numb = 1000.0;
        numb = Math.nextUp(Double.MIN_VALUE);
        /*while(numb<Double.MAX_VALUE){
            numb = Math.nextUp(numb);
            System.out.println(numb);
        }*/
        System.out.println(numb);
        System.out.println("MAX: " + Double.MAX_VALUE);
        System.out.println("MIN: " + Double.MIN_VALUE);
    }

    public static void ex5(){
        System.out.println("MAX_INT = "+Integer.MAX_VALUE);
        double maxInt = Integer.MAX_VALUE;
        double numbD = maxInt + 200;
        System.out.println("DOUBLE = "+numbD);
        int numbInt = (int) numbD;
        System.out.println("INT = "+numbInt);
    }

    public static void ex6(){
        int numb = 0;
        System.out.println("Number: ");
        Scanner in = new Scanner(System.in);
        numb = in.nextInt();
        if(numb==0){
            System.out.println(1);
        }else if(numb==1) {
            System.out.println(1);
        }else {
            BigInteger n = new BigInteger("1");
            for (int i =1; i <=numb; i++){
                n = n.multiply(BigInteger.valueOf(i));
            }
            System.out.println(n);
        }
    }

    public static void ex7(){  //ОЙ)
/*        System.out.println("Enter numbers [0; 65535]: ");
        Scanner in = new Scanner(System.in);
        String str1 = in.next();
        System.out.println(str1);
        short x = (short)(Integer.parseInt(str1)<<2);
        //System.out.println(Short.toUnsignedInt(Short.parseShort(in.next())));
        //System.out.println(Short.toUnsignedInt(Short.parseShort(in.next())));
        System.out.println(x);*/
        short s = (short) 65000;
        System.out.println(s);
        System.out.println(s & 0xffff);
        //short a = ;
        /*System.out.println(Integer.toBinaryString(8));
        short b = (short) (32767+1);
        System.out.println((b)+ ":"+Integer.toBinaryString((int)b));
        System.out.println(Integer.toString(Short.MAX_VALUE, 2));
        System.out.println(Integer.toString(b, 2));
        */
        int c = 0b1000000000000000_0000001000011000;
        System.out.println((short)c);

    }

    public static void ex8(){
        System.out.print("Str: ");
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        String[] arr;
        arr = str.split(" ");
        for(int i = 0; i< arr.length; i++){
            System.out.println(arr[i]);
        }
    }

    public static void ex10(){
        int col = 2;
        Random generate = new Random();
        StringBuilder string = new StringBuilder();
        for (int i = 0; i < col; i++) {
            long a = generate.nextLong();
            String str = Long.toString(a, 36);
            string.append(str);
        }
        System.out.println(string);
    }

    public static void ex13(){
        ArrayList<Integer> arr = new ArrayList<>();
        for (int i = 1; i <=49 ; i++) {
            arr.add(i);
        }
        System.out.println("Все: "+arr);
        final int min = 1;
        final int max = 49;
        Random generator = new Random();
        ArrayList<Integer> arrDel = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            int a = (int) (Math.random()*(50-1)+1);
            arrDel.add(a);
        }
        System.out.print("\n");
        System.out.println("Удалить: "+arrDel);
        for (int i = 0; i < 6; i++) {
            arr.remove(arrDel.get(i));
        }
        System.out.println("Результат: "+arr);
    }

    public static void ex2(){
        System.out.print("Enter angle: ");
        Scanner in = new Scanner(System.in);
        int angle = in.nextInt();

        System.out.println(angle%360);
        System.out.println(Math.floorMod(angle, 360));
    }

    public static void ex11(){
        Scanner scanner = new Scanner(System.in);
        String a = scanner.nextLine();
        char[] b = a.toCharArray();
        for (int i = 0; i < b.length;i++){
            if ((int)b[i] > 127) System.out.println(b[i] + " Unicode: \\u" + Integer.toHexString((int) b[i]));

        }
    }

    public static void ex14(){
        System.out.print("Размерность: ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Scanner in1 = new Scanner(System.in);
        String[] str = new String[n];
        for (int i = 0; i < n; i++) {
            str[i] = in1.nextLine();
        }
        System.out.println(Arrays.deepToString(str));
        //есть все данные в строках
        int[][] arr = new int [n][n];
        for (int i = 0; i < n; i++) {
            String[] a =  (str[i].split(" "));
            for (int j = 0; j < n; j++) {
                arr[i][j] = Integer.parseInt(a[j]);
            }
        }
        System.out.println(Arrays.deepToString(arr));

        int sumContr = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if(i ==j)sumContr += arr[i][j];
            }
        }
        System.out.println(sumContr);

        boolean arg = true;
        int sum = 0;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                sum += arr[i][j];
            }
            if(sum!= sumContr) {
                arg = false;
                break;
            }
            System.out.println(sum);
            sum = 0;
        }

        if(arg == true){
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    sum += arr[j][i];
                }
                if(sum!= sumContr) {
                    arg = false;
                    break;
                }
                System.out.println(sum);
                sum = 0;
            }
        }


        if(arg == true){
            for (int i = 0,  j = n-1; i < n; i++, j--) {
                sum += arr[i][j];
            }
            System.out.println(sum);
            if(sum!= sumContr) {
                arg = false;
            }
            sum = 0;
        }

        if(arg == true) System.out.println("magic");
        else {
            System.out.println("no magic");
        }
    }

    public static void triangle(){
        System.out.print("n = ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[][] triangle = new int[n][];

        for (int i = 0; i < n; i++) {
            triangle[i] = new int[i+1];
            triangle[i][0] = 1;
            triangle[i][i] = 1;
            for (int j = 1; j < i; j++) {
                triangle[i][j] = triangle[i - 1][j - 1] + triangle[i - 1][j];
            }
        }
        System.out.println(Arrays.deepToString(triangle));
    }

    public  static void ex15(){
        System.out.print("n = ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        ArrayList<ArrayList<Integer>> arrayList = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            arrayList.add(new ArrayList<>(n));
            arrayList.get(i).add    (0, 1);
            if(i!=0){
                arrayList.get(i).add(1, 1);
            }
            for (int j = 1; j < i; j++) {
                arrayList.get(i).add(j, arrayList.get(i - 1).get(j - 1) + arrayList.get(i - 1).get(j));

            }
        }
        for (int i = 0; i < n; i++) {
            System.out.println(arrayList.get(i));
        }
    }

    public static double average(double b, double... a){
        double sum = b;
        for (int i = 0; i<a.length; i++){
            sum+=a[i];
        }
        return sum/(a.length+1);
    }




    public static void main(String[] args) {
        //System.out.println(average(1));
        LocalDate date = LocalDate.now().withDayOfMonth(1); //какой день сейчас, начиная с 1 дня месяца
        int month;
        DayOfWeek weekday = date.getDayOfWeek();
        System.out.println(weekday);
        int value = weekday.getValue(); // 1 = Monday, ... 7 = Sunday
        System.out.println(value);


    }
}
